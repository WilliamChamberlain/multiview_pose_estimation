function [camera_n_pose_n , cam_coordinates] = cam_n_pose_n_coordinates() 

% image_dir=fullfile('H:','Documents','ownCloud','project_AA1__1_1');
%image_dir=fullfile('D:','_will','ownCloud','project_AA1__1_1');

camera_n_pose_n = imread(fullfile(image_dir,'camera_n_pose_n (m).jpg'));

imshow(camera_n_pose_n);

% image is already rectified

% lost the camera locations - phone crashed



cam_whiteboard_on_right=[ 
    not_observed();     % top left
    not_observed();     % top right
    not_observed();           % bottom right
    not_observed();     % bottom left
    ];

cam_whiteboard_on_left=[
    not_observed();  % top left
    not_observed();  % top right
    not_observed();   % bottom right
    not_observed();   % bottom left
    ];

cam_door=[
    not_observed();     % top left inner corner
    not_observed();     % top right inner corner
    not_observed();     % bottom right inner corner
    not_observed();        % bottom left inner corner
    ];

cam_emergency_stop_flush= [
    not_observed();         % top left, flush to wall
    not_observed();         % top right, flush to wall
    not_observed();
    not_observed();
    ];
cam_emergency_stop_proud= [
    not_observed();     % top left, away from wall
    not_observed();     % top right, away from wall
    not_observed();         % bottom right away from wall
    not_observed();         % bottom left away from wall
    ];

cam_light_switch= [
    not_observed();      % top left, flush to wall
    not_observed();      % top right, flush to wall
    not_observed();      % bottom right flush to wall
    not_observed();      % bottom left flush to wall
    ];
    
cam_table_on_right= [ 
    not_observed();     % rear left
    not_observed();        % rear right
    not_observed();        % front right
    not_observed();        % front left
    ];

cam_rear_table_on_left = [
    not_observed();     % rear left
    not_observed();         % rear right
    not_observed();         % front right
    not_observed();         % front left
    ];

cam_centre_table_on_left = [
    not_observed();   % rear left
    not_observed();   % rear right
    not_observed();   % front right
    not_observed();  % front left
    ];

cam_coordinates = vertcat( cam_whiteboard_on_right , cam_whiteboard_on_left , cam_door , cam_emergency_stop_flush , cam_emergency_stop_proud , cam_light_switch , cam_table_on_right , cam_rear_table_on_left , cam_centre_table_on_left );

end