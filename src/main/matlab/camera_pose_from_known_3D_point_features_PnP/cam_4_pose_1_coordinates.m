function [camera_4_pose_1 , cam_coordinates] = cam_4_pose_1_coordinates(image_dir_) 

% image_dir=fullfile('H:','Documents','ownCloud','project_AA1__1_1');
%image_dir=fullfile('D:','_will','ownCloud','project_AA1__1_1');
if ~exist('image_dir_','var') , image_dir_ = pwd(); end;

image_filename = 'camera_4_pose_1 (1).jpg';
camera_4_pose_1 = imread(fullfile(image_dir_,image_filename));

image_fig_h = figure('Name',image_filename);
imshow(camera_4_pose_1);

% image is already rectified

% lost the camera locations - phone crashed



cam_whiteboard_on_right=[ 
    360 0039;     % top left
    2045 0014;     % top right
    not_observed();           % bottom right
    not_observed();     % bottom left
    ];

cam_whiteboard_on_left=[
    not_observed();  % top left
    360 0039;  % top right
    not_observed();   % bottom right
    not_observed();   % bottom left
    ];

cam_door=[
    2344 0043;     % top left inner corner
    3106 0025;     % top right inner corner
    2841 1483;     % bottom right inner corner
    not_observed();        % bottom left inner corner
    ];

cam_emergency_stop_flush= [
    not_observed();         % top left, flush to wall
    not_observed();         % top right, flush to wall
    not_observed();
    not_observed();
    ];
cam_emergency_stop_proud= [
    2126 779;     % top left, away from wall
    not_observed();     % top right, away from wall
    not_observed();         % bottom right away from wall
    not_observed();         % bottom left away from wall
    ];

cam_light_switch= [
    3065 731;      % top left, flush to wall
    not_observed();      % top right, flush to wall
    not_observed();      % bottom right flush to wall
    3050 809;      % bottom left flush to wall
    ];
    
cam_table_on_right= [ 
    2982 1099;     % rear left
    not_observed();        % rear right
    not_observed();        % front right
    not_observed();        % front left
    ];

cam_rear_table_on_left = [
    not_observed();     % rear left
    1460 1043;         % rear right
    1478 1330;         % front right
    0043 1407;         % front left
    ];

cam_centre_table_on_left = [
    not_observed();   % rear left
    not_observed();   % rear right
    not_observed();   % front right
    not_observed();  % front left
    ];

cam_coordinates = vertcat( cam_whiteboard_on_right , cam_whiteboard_on_left , cam_door , cam_emergency_stop_flush , cam_emergency_stop_proud , cam_light_switch , cam_table_on_right , cam_rear_table_on_left , cam_centre_table_on_left );

end