
function [world_coordinates, object_vertices]= world_coordinates_setup()
% assumes that all objects have the same number of vertices = 4 : all
% objects are mapped as their upper front vertices from the camera POV ;
% if sets of vertices with the same z i.e. parallel to the floor give the vertices with max z; top corners of desks
% if sets of vertices with the same y i.e. parallel to the back wall, give
% the vertices with the lowest y; door frames - which is why the emergency
% stop is modelled as two objects.
% object_vertices: can get the object's vertices as squeeze(object_vertices(1,:,:))
%
%
% LESSON LEARNT - always use visual markers that I can template off to get
% authoritative ground truth points, 
% and use them for ground truth of multiple grid points on the floor etc
% e.g. take a shot with markers in all useful places, then take another
% shot with the markers removed.
% Something distinctive like two concentric circles, a 2-digit number in
% the centre, an arrow in the direction of a point of interest - stem of
% arrow length = one radius , and arrow head is equilateral of side = one
% radius
% Then can warp+template to detect, and OCR to get the number --> struct

room_depth              = 643.5;
room_width              = 780.0;
% nominal_zero            = [158+190+190 0 0];
nominal_zero            = [room_width room_depth 0];

skirting_board_depth    = 3; 
skirting_board_height   = 14.5;

whiteboard_on_right=[ 
    158+190 0 210;  % top left
    158 0 210;      % top right
    158 0 90;       % bottom right
    158+190 0 90;   % bottom left
    ];

whiteboard_on_left=[
    158+190+190 0 210;  % top left
    158+190     0 210;  % top right
    158+190     0 90;   % bottom right
    158+190+190 0 90;   % bottom left
    ];

door=[
    125, 0,205.5;       % top left inner corner
    29.5,0,205.5;       % top right inner corner
    29.5,0,0;           % bottom right inner corner
    125, 0,0;           % bottom left inner corner
    ];

emergency_stop_flush= [
    142, 0,115;      % top left, flush to wall
    132, 0,115;      % top right, flush to wall
    132, 0,105;      % bottom right flush to wall
    142, 0,105;      % bottom left flush to wall
    ];
emergency_stop_proud= [
    142, 7,115;      % top left, away from wall
    132, 7,115;      % top right, away from wall
    132, 7,105;      % bottom right away from wall
    142, 7,105;      % bottom left away from wall
    ];

light_switch= [
    17, 0,116;      % top left, flush to wall
    9.5,0,116;      % top right, flush to wall
    9.5,0,104.5;      % bottom right flush to wall
    17 ,0,104.5;      % bottom left flush to wall
    ];
    
table_on_right= [ 
    70 83.5     105; % rear left
    -5 83.5     105; % rear right
    -5 83.5+150 105; % front right
    70 83.5+150 105; % front left
    ];

rear_table_on_left = [
    231+150,0,  80; % rear left
    231,    0,  80; % rear right
    231,    75, 80; % front right
    231+150,75, 80; % front left
    ];

centre_table_on_left = [
    244.5+75,177.5,     79.5;   % rear left
    244.5,  177.5,      79.5;   % rear right
    244.5,  177.5+150,  79.5;   % front right
    244.5+75,177.5+150,  79.5;  % front left
    ];


world_coordinates = vertcat( whiteboard_on_right , whiteboard_on_left , door , emergency_stop_flush , emergency_stop_proud , light_switch , table_on_right , rear_table_on_left , centre_table_on_left );

% x_corrected_coordinates = coordinates_correct_for_reflection(world_coordinates,nominal_zero,1);
world_coordinates(:,1)=(ones(size(world_coordinates,1),1).*nominal_zero(1))-world_coordinates(:,1);

% xy_corrected_coordinates = coordinates_correct_for_reflection(world_coordinates,nominal_zero,2);
world_coordinates(:,2)=(ones(size(world_coordinates,1),1).*nominal_zero(2))-world_coordinates(:,2);


object_vertices = cat(3, ...
    correct_coords(whiteboard_on_right, nominal_zero)   , ...
    correct_coords(whiteboard_on_left, nominal_zero) , ...
    correct_coords(door, nominal_zero)                  , ...
    correct_coords(emergency_stop_flush, nominal_zero) , ...
    correct_coords(emergency_stop_proud, nominal_zero)  , ...
    correct_coords(light_switch, nominal_zero) , ... 
    correct_coords(table_on_right, nominal_zero)        , ...
    correct_coords(rear_table_on_left, nominal_zero) , ...
    correct_coords(centre_table_on_left, nominal_zero)  );
object_vertices = correct_coords(object_vertices, nominal_zero);
object_vertices = permute(object_vertices,[3,1,2]);
% now can get the object's vertices as squeeze(object_vertices(1,:,:))




end
