if ~exist('plot_figures') , plot_figures = true; end;
% calibrate_phone_cam.m --> cameraParams.IntrinsicMatrix
if ~exist('cameraParams') , [cameraParams, imagesUsed, estimationErrors] = calib_3_coefficient(); end;
%object_coordinates.m --> world_coordinates - x,y,z world coordinates in cm
if ~exist('world_coordinates') , world_coordinates = world_coordinates_setup(); end;
%camera_locations.m  --> cam_coordinates - u,v pixel coordinates
% image_dir= 'D:\_will\ownCloud\project_AA1__1_1';
[cam_4_pose_1 , cam_coordinates]  = cam_4_pose_1_coordinates(image_dir);

observed_world_coordinates = world_coordinates(~is_not_observed(cam_coordinates(:,1)),:);
observed_world_coordinates = horzcat(observed_world_coordinates , ones(size(observed_world_coordinates,1),1));
observed_cam_coordinates   = cam_coordinates(~is_not_observed(cam_coordinates(:,1)),:);
observed_cam_coordinates   = horzcat(observed_cam_coordinates , ones(size(observed_cam_coordinates,1),1)); 

if plot_figures
    figure('Name','cam_4_pose_1: image and measured pixel coordinates');
    imshow(cam_4_pose_1);
    hold on;
    plot(observed_cam_coordinates(:,1),observed_cam_coordinates(:,2),'go')

    figure('Name','cam_4_pose_1: world_coordinates in 3D');
    plot3(world_coordinates(:,1),world_coordinates(:,2),world_coordinates(:,3),'rx')
end

[R,T,Xc,best_solution]=efficient_pnp(observed_world_coordinates, observed_cam_coordinates, cameraParams.IntrinsicMatrix');  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
%Transform = [R T; 0 0 0 1];

[Ri,Ti,Xci,best_solutioni]=efficient_pnp(observed_world_coordinates, observed_cam_coordinates, eye(3,3));  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

[Rgauss,Tgauss,Xcgauss,best_solutiongauss]=efficient_pnp_gauss(observed_world_coordinates, observed_cam_coordinates, cameraParams.IntrinsicMatrix');  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

[Rgaussi,Tgaussi,Xcgaussi,best_solutiongaussi]=efficient_pnp_gauss(observed_world_coordinates, observed_cam_coordinates, eye(3,3));  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

%figure; hold on;

% error=reprojection_error_usingRT(Xw,U,Rp,Tp,A);

camera_pose = -(R')*T
    if plot_figures
    hold on;
    plot3(camera_pose(1,1),camera_pose(2,1),camera_pose(3,1),'go');
end

camera_pose_4_1 = camera_pose;