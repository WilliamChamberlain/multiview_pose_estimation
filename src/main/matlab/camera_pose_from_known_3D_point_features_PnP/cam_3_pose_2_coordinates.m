function [camera_3_pose_2 , cam_coordinates] = cam_3_pose_2_coordinates() 

% image_dir=fullfile('H:','Documents','ownCloud','project_AA1__1_1');
image_dir=fullfile('D:','_will','ownCloud','project_AA1__1_1');

camera_3_pose_2 = imread(fullfile(image_dir,'camera_3_pose_2 (1).jpg'));

imshow(camera_3_pose_2);

% image is already rectified

% lost the camera locations - phone crashed



cam_whiteboard_on_right=[ 
    596 317;     % top left
    1736 328;     % top right
    not_observed();           % bottom right
    not_observed();     % bottom left
    ];

cam_whiteboard_on_left=[
    not_observed();  % top left
    596 317;  % top right
    not_observed();   % bottom right
    not_observed();   % bottom left
    ];

cam_door=[
    1924 351;     % top left inner corner
    2372 349;     % top right inner corner
    2279 1338;     % bottom right inner corner
    not_observed();        % bottom left inner corner
    ];

cam_emergency_stop_flush= [
    not_observed();         % top left, flush to wall
    not_observed();         % top right, flush to wall
    not_observed();
    not_observed();
    ];
cam_emergency_stop_proud= [
    not_observed();     % top left, away from wall
    not_observed();     % top right, away from wall
    not_observed();         % bottom right away from wall
    not_observed();         % bottom left away from wall
    ];

cam_light_switch= [
    not_observed();      % top left, flush to wall
    not_observed();      % top right, flush to wall
    not_observed();      % bottom right flush to wall
    not_observed();      % bottom left flush to wall
    ];
    
cam_table_on_right= [ 
    2357 990;     % rear left
    not_observed();        % rear right
    not_observed();        % front right
    3009 1347;        % front left
    ];

cam_rear_table_on_left = [
    not_observed();     % rear left
    1338 1043;         % rear right
    1430 1186;         % front right
    408 1274;         % front left
    ];

cam_centre_table_on_left = [
    not_observed();   % rear left
    1559 1475;   % rear right
    not_observed();   % front right
    not_observed();  % front left
    ];

cam_coordinates = vertcat( cam_whiteboard_on_right , cam_whiteboard_on_left , cam_door , cam_emergency_stop_flush , cam_emergency_stop_proud , cam_light_switch , cam_table_on_right , cam_rear_table_on_left , cam_centre_table_on_left );

end