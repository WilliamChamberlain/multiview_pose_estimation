
    function coords_new = correct_coords(coords_, nominal_zero)
        coords_new     = coords_;
        coords_new(:,1)= (ones(size(coords_new,1),1).*nominal_zero(1))-coords_new(:,1);
        coords_new(:,2)= nominal_zero(2)-coords_new(:,2);
    end