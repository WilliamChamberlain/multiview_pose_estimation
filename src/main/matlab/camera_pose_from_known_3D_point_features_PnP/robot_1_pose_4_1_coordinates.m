function [camera_1_pose_1 , cam_coordinates] = robot_1_pose_4_1_coordinates() 

camera_1_pose_1 = iread('/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_2016_09_07/robot_1_pose_4 (1).jpg');

imshow(camera_1_pose_1);

% image is already rectified

% lost the camera locations - phone crashed



cam_whiteboard_on_right=[ 
    625 31;     % top left
    1928 28;     % top right
    1928 851;           % bottom right
    not_observed();     % bottom left
    ];

cam_whiteboard_on_left=[
    not_observed();  % top left
    625 31;  % top right
    not_observed();   % bottom right
    not_observed();   % bottom left
    ];

cam_door=[
    2175 54;     % top left inner corner
    2828 45;     % top right inner corner
    not_observed();     % bottom right inner corner
    2175 1473;        % bottom left inner corner
    ];

cam_emergency_stop_flush= [
    2038 683;         % top left, flush to wall
    not_observed();         % top right, flush to wall
    not_observed();
    2038 750;
    ];
cam_emergency_stop_proud= [
    2042 683;     % top left, away from wall
    2108 683;     % top right, away from wall
    2108 745;         % bottom right away from wall
    2042 745;         % bottom left away from wall
    ];

cam_light_switch= [
    not_observed();      % top left, flush to wall
    not_observed();      % top right, flush to wall
    2968 745;      % bottom right flush to wall
    2912 745;      % bottom left flush to wall
    ];
    
cam_table_on_right= [ 
    2746 689;     % rear left
    not_observed();        % rear right
    not_observed();        % front right
    not_observed();        % front left
    ];

cam_rear_table_on_left = [
    not_observed();     % rear left
    1430 921;         % rear right
    1379 906;         % front right
    not_observed();         % front left
    ];

cam_centre_table_on_left = [
    not_observed();   % rear left
    1171 888;   % rear right
    148 738;   % front right
    not_observed();  % front left
    ];

cam_coordinates = vertcat( cam_whiteboard_on_right , cam_whiteboard_on_left , cam_door , cam_emergency_stop_flush , cam_emergency_stop_proud , cam_light_switch , cam_table_on_right , cam_rear_table_on_left , cam_centre_table_on_left );

end