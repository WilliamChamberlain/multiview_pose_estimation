% calibrate_phone_cam.m --> cameraParams.IntrinsicMatrix
if ~exist('cameraParams')  
    addpath('/mnt/nixbig/ownCloud/project_AA1__1_1/Nexus_5_calib/38mm_square_checkerboard');
    [cameraParams, imagesUsed, estimationErrors] = calib_3_coefficient(); 
end;
%object_coordinates.m --> world_coordinates - x,y,z world coordinates in cm
if ~exist('world_coordinates') 
    addpath('/mnt/nixbig/ownCloud/project_AA1__1_1/code/mutliview_pose_estimation/src/main/matlab/camera_pose_from_known_3D_point_features_PnP');
    addpath('/mnt/nixbig/ownCloud/project_AA1__1_1/code/mutliview_pose_estimation/src/main/matlab');
    world_coordinates = world_coordinates_setup(); 
    world_coordinates = world_coordinates_setup(); 
end;
%camera_locations.m  --> cam_coordinates - u,v pixel coordinates
[cam_n_pose_n , cam_coordinates]  = robot_1_pose_4_1_coordinates();

observed_world_coordinates = world_coordinates(~is_not_observed(cam_coordinates(:,1)),:);
observed_world_coordinates = horzcat(observed_world_coordinates , ones(size(observed_world_coordinates,1),1));
observed_cam_coordinates   = cam_coordinates(~is_not_observed(cam_coordinates(:,1)),:);
observed_cam_coordinates   = horzcat(observed_cam_coordinates , ones(size(observed_cam_coordinates,1),1)); 

figure('Name','image and measured pixel coordinates');
imshow(cam_n_pose_n);
hold on;
%plot pixel coordinates back into the image as a double-check
plot(observed_cam_coordinates(:,1),observed_cam_coordinates(:,2),'go')  

[R,T,Xc,best_solution]=efficient_pnp(observed_world_coordinates, observed_cam_coordinates, cameraParams.IntrinsicMatrix');  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
%Transform = [R T; 0 0 0 1];

[Ri,Ti,Xci,best_solutioni]=efficient_pnp(observed_world_coordinates, observed_cam_coordinates, eye(3,3));  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

[Rgauss,Tgauss,Xcgauss,best_solutiongauss]=efficient_pnp_gauss(observed_world_coordinates, observed_cam_coordinates, cameraParams.IntrinsicMatrix');  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

[Rgaussi,Tgaussi,Xcgaussi,best_solutiongaussi]=efficient_pnp_gauss(observed_world_coordinates, observed_cam_coordinates, eye(3,3));  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)

%figure; hold on;

% error=reprojection_error_usingRT(Xw,U,Rp,Tp,A);

camera_pose = -(R')*T

% plot the 3D scene plot
figure('Name','world_coordinates in 3D');
plot3(world_coordinates(:,1),world_coordinates(:,2),world_coordinates(:,3),'rx')
hold on;
% plot the robot camera pose into the 3D scene plot
plot3(camera_pose(1,1),camera_pose(2,1),camera_pose(3,1),'go');


R__robot_1_pose_4_1 = R
T__robot_1_pose_4_1 = T
robot_pose__robot_1_pose_4_1 = camera_pose


