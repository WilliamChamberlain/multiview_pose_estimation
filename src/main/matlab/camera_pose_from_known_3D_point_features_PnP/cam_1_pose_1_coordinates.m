function [camera_1_pose_1 , cam_coordinates] = cam_1_pose_1_coordinates() 


% image_dir=fullfile('H:','Documents','ownCloud','project_AA1__1_1');
% image_dir=fullfile('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_09_07');
image_dir=fullfile('/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_2016_09_07');



camera_1_pose_1 = imread(fullfile(image_dir,'camera_1_pose_1 (1).jpg'));


imshow(camera_1_pose_1);

% image is already rectified

% lost the camera locations - phone crashed



cam_whiteboard_on_right=[ 
    not_observed();     % top left
    not_observed();     % top right
    1538 589;           % bottom right
    not_observed();     % bottom left
    ];

cam_whiteboard_on_left=[
    not_observed();  % top left
    not_observed();  % top right
    not_observed();   % bottom right
    not_observed();   % bottom left
    ];

cam_door=[
    not_observed();     % top left inner corner
    not_observed();     % top right inner corner
    not_observed();     % bottom right inner corner
    1723 , 1013;        % bottom left inner corner
    ];

cam_emergency_stop_flush= [
    1622 , 454;         % top left, flush to wall
    1673 , 452;         % top right, flush to wall
    not_observed();
    not_observed();
    ];
cam_emergency_stop_proud= [
    not_observed();     % top left, away from wall
    not_observed();     % top right, away from wall
    1676 , 511;         % bottom right away from wall
    1625 , 512;         % bottom left away from wall
    ];

cam_light_switch= [
    2290 , 433;      % top left, flush to wall
    2328 , 433;      % top right, flush to wall
    2323 , 493;      % bottom right flush to wall
    not_observed();      % bottom left flush to wall
    ];
    
cam_table_on_right= [ 
    not_observed();     % rear left
    2549 ,  615;        % rear right
    3009 , 1003;        % front right
    2352 , 1016;        % front left
    ];

cam_rear_table_on_left = [
    not_observed();     % rear left
    1158 , 639;         % rear right
    1112 , 774;         % front right
    175  , 790;         % front left
    ];

cam_centre_table_on_left = [
    not_observed();   % rear left
    974  , 1041;   % rear right
    616  , 1808;   % front right
    not_observed();  % front left
    ];

cam_coordinates = vertcat( cam_whiteboard_on_right , cam_whiteboard_on_left , cam_door , cam_emergency_stop_flush , cam_emergency_stop_proud , cam_light_switch , cam_table_on_right , cam_rear_table_on_left , cam_centre_table_on_left );

end