function [features_,descriptors_] = detect_and_draw_sift_features(image_, figure_handle_,edge_threshold_,peak_threshold_)
% Test/demo wrapper for vl_sift(image_,
% figure_handle_,edge_threshold_,peak_threshold_) 
% and displaying the image with detected SIFT features
% See http://matlab.wikia.com/wiki/FAQ#How_do_I_create_a_circle.3F

    image_gray = single(rgb2gray(image_));   % mine - peak_threshold cuts features to zero over 10
%     image_gray = rgb2gray(im2single(image_));  % vl_feat - vlfeat_sift_mosaic_peakthreshold  % - peak_threshold cuts features to zero between 0 and 2
    [features_,descriptors_] = vl_sift(image_gray,'EdgeThresh',edge_threshold_, 'PeakThresh', peak_threshold_);
    is_holding = ishold;
    figure(figure_handle_);
    imshow(image_);
    hold on; 
    viscircles( [ features_(1,:);features_(2,:)]' , features_(3,:),'Color','green','LineWidth',1);
    if ~is_holding , hold off; end;
end


% fighandle17=figure('Name','camera_1_pose_1_gray 17');
% imshow(camera_1_pose_1)
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2);
% fighandle18=figure('Name','camera_1_pose_1_gray 18');
% imshow(camera_1_pose_1)
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2, 20);
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2, 30);
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2, 25);
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2, 15);
% detect_and_draw_sift_features(camera_1_pose_1,fighandle17, 2, 10);

