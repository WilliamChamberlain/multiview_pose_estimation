%{
z  hostname                    IP              MAC
50 android-8a1640242815d6e8    192.168.1.227   b4:79:a7:1d:b4:2f
70 android-21da83a7e65e9940    192.168.1.131   b4:79:a7:1d:b4:65
100 android-b68ae593574701f9   192.168.1.136   b4:79:a7:1d:b0:9c

Initial position: 
facing bookcase=90 degrees anticlockwise from y axis (y axis from robot lab to cantina), 
y0=camera offset from wall = 44-41cm left of frame, 
x=camera offset from bookcase=301cm.
theta = 90

2nd 
y     = whatever
x     = 285 (not 301)
theta = 60

3rd 
y     = whatever
x     = 278 (not 301)
theta = 30

4th
y     = whatever
x     = 285
theta = 120



Apriltags centres on bookcase: 
    x :  0
    y : 13cm, 103cm, 193cm,   13cm, 103cm, 193cm
    z : 54cm,  54cm,   54cm, 144cm, 144cm, 144cm
on floor: 
    x : 38cm
    y : 124+180cm
    z : 18cm
on circusbot:
    x : 210 + 38
    y : 687 - 37
    z : 29
on door frame
    x : 12
    y : 270 + 88 = 358
    z : 188
%}

function [cctv_1_fhandle,cctv_2_fhandle,cctv_3_fhandle] = record_ip_webcams(x_, y_, theta_,cctv_1_fhandle,cctv_2_fhandle,cctv_3_fhandle)
    if ~exist('cctv_1_fhandle','var'), cctv_1_fhandle = figure('Name','cctv_1 plain'); end;
    date_time_str = datestr(now(),'yyyymmddHHMM');
    file_dir = '/mnt/nixbig/ownCloud/project_AA1__1_1/results/2016_11_29_1820_S11_bookcase_imageset/';
    file_suffix = 'tiff';
    cctv_1_im_url       = 'http://192.168.1.227:8080/shot.jpg'; 
    cctv_1_im = imread(cctv_1_im_url); 
    cctv_1_info = imfinfo(cctv_1_im_url); 
    cctv_1_data = webread(cctv_1_im_url);   
    figure(cctv_1_fhandle); imshow(cctv_1_im);
    cam_name = 'cctv_1'
    z = 50
    file_name = sprintf('%s%d_%d_%d_%d_%s_%s.%s',file_dir,x_,y_,z,theta_,cam_name,date_time_str,file_suffix)
    imwrite(cctv_1_im, file_name,'Compression','none','Resolution',100)     
    
    if ~exist('cctv_2_fhandle','var'), cctv_2_fhandle = figure('Name','cctv_2 plain'); end;
    cctv_2_im_url       = 'http://192.168.1.131:8080/shot.jpg'; 
    cctv_2_im = imread(cctv_2_im_url); 
    cctv_2_info = imfinfo(cctv_2_im_url); 
    cctv_2_data = webread(cctv_2_im_url);   
    figure(cctv_2_fhandle); imshow(cctv_2_im);
    cam_name = 'cctv_2'
    z = 70
    file_name = sprintf('%s%d_%d_%d_%d_%s_%s.%s',file_dir,x_,y_,z,theta_,cam_name,date_time_str,file_suffix)
    imwrite(cctv_2_im, file_name,'Compression','none','Resolution',100)     
    
    if ~exist('cctv_3_fhandle','var'), cctv_3_fhandle = figure('Name','cctv_3 plain'); end;
    cctv_3_im_url       = 'http://192.168.1.136:8080/shot.jpg'; 
    cctv_3_im = imread(cctv_3_im_url); 
    cctv_3_info = imfinfo(cctv_3_im_url); 
    cctv_3_data = webread(cctv_3_im_url);   
    figure(cctv_3_fhandle); imshow(cctv_3_im);
    cam_name = 'cctv_3'
    z = 100
    file_name = sprintf('%s%d_%d_%d_%d_%s_%s.%s',file_dir,x_,y_,z,theta_,cam_name,date_time_str,file_suffix)
    imwrite(cctv_3_im, file_name,'Compression','none','Resolution',100) 
end

