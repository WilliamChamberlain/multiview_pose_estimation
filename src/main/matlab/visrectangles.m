function visrectangles(top, bottom, left, right)
    for num_input = 1 : size(top,2)
        vertices = rect_vertices(top(num_input) , bottom(num_input) , left(num_input) , right(num_input), 'for_plot');
        line(vertices(1,:),vertices(2,:));
    end
end