function image__ = trim_image(image_, varargin)    
    display(sprintf('length(varargin)=%d',length(varargin)));
    num_border_pixels = 0;
    for i=1:2:length(varargin)  % parse arguments
       switch varargin{i}
       case 'border_absolute'
           display('absolute');
         num_border_pixels = varargin{i+1};
           display(sprintf('num_border_pixels = %d',num_border_pixels));
       case 'border_percentage'
           display('perc');
         if image_size(1) < image_size(2)
            image_max_dim = image_size(1);
            display(sprintf('image_size(1) < image_size(2) : image_max_dim = %d',image_max_dim));
         else
            image_max_dim = image_size(2);             
            display(sprintf('else : image_max_dim = %d',image_max_dim));
         end
         num_border_pixels = image_max_dim*(varargin{i+1}/100);
           display(sprintf('num_border_pixels = %d',num_border_pixels));
       default
         error(sprintf('%s is not a valid argument name',varargin{i}));
       end
    end
   image_size = size(image_);
   image__ = image_( ...
       num_border_pixels:image_size(1)-num_border_pixels , ...
       num_border_pixels:image_size(2)-num_border_pixels , ...
       : );
end