
function is_not_observed_ = is_not_observed(observation_)
    is_not_observed_ = isnan(observation_);
end

