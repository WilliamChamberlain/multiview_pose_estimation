%{
Neural network features for pose estimation

https://au.mathworks.com/help/nnet/ref/seriesnetwork.activations.html 

https://au.mathworks.com/help/vision/examples/image-category-classification-using-deep-learning.html#zmw57dd0e1313
    -->  Load Pre-trained CNN

Stephanie Lowry: 
p5:
D. Appearance change and viewpoint change
The previous experiments used images that were captured
from the same viewpoint, so that evaluation of the inlier
matches could be easily performed. However, the real concern
is the system performance when the viewpoint of the camera
has changed. The final experiment demonstrates performance
on a dataset that demonstrates both viewpoint change and
appearance change. The image set was a set of images of
Notre Dame Cathedral from the well-known Paris Buildings
dataset [28] as shown in Figure 8. The images used represent a
range of different viewpoint offsets and day-night appearances.
1) Setup: Once again the SURF feature detector was used,
with 1000 features extracted per image. Four feature descrip-
tion techniques were tested – 128-dimension SURF features,
image patches resized to 121 dimensions to provide a compa-
rably sized feature to the 128-dimension SURF features, 7056-
dimension HOG features as used in the previous experiments,
and 64896-dimension conv3 features were extracted as in
[33] [Niko S ̈underhauf, Sareh Shirazi, Adam Jacobson, Feras
Dayoub, Edward Pepperell, Ben Upcroft, and Michael Milford. 
Place recognition with ConvNet landmarks: Viewpoint-robust, 
condition-robust, training-free.] 
using the Caffe framework [12] [Yangqing Jia, Evan Shelhamer, Jeff Donahue, Sergey
Karayev, Jonathan Long, Ross Girshick, Sergio Guadarrama, and Trevor Darrell. 
Caffe: Convolutional architecture for fast feature embedding.]
and the default pre-trained
ImageNet model [14] [Alex Krizhevsky, Ilya Sutskever, and Geoffrey Hinton.
ImageNet classification with deep convolutional neural networks.]
. 
In each case, the upright feature was
used. 
Following [17] [S. Lowry and M. J. Milford. 
Supervised and unsupervised linear learning techniques for visual place 
recognition in changing environments]
, we learned a PCA decomposition for
each feature type on a training image and post-processed the
extracted features using Zero Components Analysis (ZCA) to improve condition robustness.


-------------------------------------------------------------------------

%}

% Get GPU device information
deviceInfo = gpuDevice;

% Check the GPU compute capability
computeCapability = str2double(deviceInfo.ComputeCapability);
assert(computeCapability > 3.0, ...
    'This example requires a GPU device with compute capability 3.0 or higher.');



%% https://au.mathworks.com/help/vision/examples/image-category-classification-using-deep-learning.html#zmw57dd0e1313
%% http://www.vlfeat.org/matconvnet/models/
%%  http://www.vlfeat.org/matconvnet/pretrained/
%% http://www.vlfeat.org/matconvnet/functions/
%% 
% TODO for some reason the Matlab implementation does not suppport some of
% the vlfeat pre-trained ; worth looking at using matconvnet rather than
% using Matlab?
convnet = helperImportMatConvNet('/mnt/nixbig/downloads/pretrained networks matlab vlfeat/imagenet-caffe-alex.mat')

% Load MatConvNet network into a SeriesNetwork
convnet = helperImportMatConvNet('/mnt/nixbig/downloads/pretrained networks matlab vlfeat/imagenet-caffe-alex.mat')

convnet.Layers

convnet.Layers(1)

convnet.Layers(end)
numel(convnet.Layers(end).ClassNames)





