%SurfPointFeature.match Match SURF point features
        %   
        % M = F.match(F2, OPTIONS) is a vector of FeatureMatch objects that 
        % describe candidate matches between the two vectors of SURF 
        % features F and F2.  Correspondence is based on descriptor
        % similarity.
        %
        % [M,C] = F.match(F2, OPTIONS) as above but returns a correspodence
        % matrix where each row contains the indices of corresponding features
        % in F and F2  respectively.
        %
        % Options::
        % 'thresh',T    Match threshold (default 0.05)
        % 'median'      Threshold at the median distance
        %
        % Notes::
        % - for no threshold set to [].
        %
        % See also FeatureMatch.
        
        function [m,corresp] = matchSurfPointFeaturesCos(f1, f2, varargin)
%         Duplicate Peter's SurfPointFeature.match, so can then extend and
%         modify it.
            if isempty(f2)
                m = [];
                corresp = [];
                return;
            end

            opt.thresh = 0.05;
            opt.median = false;
            opt = tb_optparse(opt, varargin);

            % Put the landmark descriptors in a matrix
            D1 = f1.descriptor;
            D2 = f2.descriptor;

            % Find the best matches
            err=zeros(1,length(f1));
            cor1=1:length(f1); 
            cor2=zeros(1,length(f1));
            for i=1:length(f1),
%                 distance = sum((D2-repmat(D1(:,i),[1 length(f2)])).^2,1);
                distance = zeros(1,length(f2),'double');
                for d2_idx_=1:length(f2),
                    distance(d2_idx_) = cosineSimilarity(D1(:,i)',D2(:,d2_idx_)');
                end
                [err(i),cor2(i)] = min(distance);
            end

            % Sort matches on vector distance
            [err, ind] = sort(err); 
            cor1=cor1(ind); 
            cor2=cor2(ind);

            % Build a list of FeatureMatch objects
            m = [];
            cor = [];
            for i=1:length(f1)
                k1 = cor1(i);
                k2 = cor2(i);
                mm = FeatureMatch(f1(k1), f2(k2), err(i));
                m = [m mm];
                cor(:,i) = [k1 k2]';
            end            

            % get the threshold, either given or the median of all errors
            if opt.median
                thresh = median(err);
                display(sprintf('Median error threshold = median error = %d', thresh));
            else
                thresh = opt.thresh;
                display(sprintf('Fixed error threshold = %d', thresh));
            end

            % remove those matches over threshold
            if ~isempty(thresh)
                k = err > thresh;
                cor(:,k) = [];
                m(k) = [];
            end

            if nargout > 1
                corresp = cor;
            end
        end