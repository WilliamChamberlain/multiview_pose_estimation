function vertices_ = rect_vertices(top, bottom, left, right, varargin)
% Return vertices top-left, top-right, bottom-right, bottom-left; clockwise
% from top left.
%
    vertices_ = [ [left;top] , [right; top] , [right; bottom] , [left; bottom]];
    if 1 <= length(varargin) 
        inputExist = find(cellfun(@(x) strcmpi(x, 'for_plot') , varargin));
%         if inputExist
%           fontsize = varargin{inputExist+1};
%         end
        if inputExist,  vertices_ = horzcat( vertices_ , [left;top]);  end;
    end;
end