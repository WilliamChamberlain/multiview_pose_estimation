function test_draw_subimage_borders_with_overlap(im_, varargin)
    vertical_divs=5;
    horizontal_divs=5;
    overlap_px = 10;
    sub_image_height=ceil(size(im_,1)/horizontal_divs);
    sub_image_width=ceil(size(im_,2)/horizontal_divs);
    
    if length(varargin) > 0
        colour_ = varargin{1};
    else 
        colour_ = 'g';
    end
        

    for vertical_div = 0:vertical_divs-1
        if vertical_div == 0
            sub_image_top = vertical_div*sub_image_height;
        else
            sub_image_top = vertical_div*sub_image_height - overlap_px;
        end
        if vertical_div < vertical_divs-1
            sub_image_bottom = (vertical_div+1)*sub_image_height + overlap_px;
        else
            sub_image_bottom = size(im_,1);
        end
        for horizontal_div = 0:horizontal_divs-1
            if horizontal_div > 0 
                sub_image_left = horizontal_div*sub_image_width - overlap_px;
            else
                sub_image_left = horizontal_div*sub_image_width;
            end
            if horizontal_div < horizontal_divs-1
                sub_image_right = (horizontal_div+1)*sub_image_width + overlap_px;
            else
                sub_image_right = size(im_,2);
            end
            plot([sub_image_left sub_image_right sub_image_right sub_image_left] , ...
                [sub_image_top sub_image_top sub_image_bottom sub_image_bottom], colour_);
        end
    end
end