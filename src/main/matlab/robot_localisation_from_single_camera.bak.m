% [Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose')

% Estimate via essential or fundamental matrices from SURF matches of two
% views of S11 labs
% Matching - Robotics, Vision and Control pp385-6
%   parameters are
%       distance measure
%       threshold
%           Robotics, Vision and Control pp385-6
%               threshold options: 'top', 'thresh', 'median'
%               descriptor matching threshold: zero-mean normalized cross-correlation ZNCC gives correspondence strength between descriptors of points - Robotics, Vision and Control pp383-384
% Use for estimation
%   five point from Vincent
%   eight point from Hartley
%       Peter's
% Use RANSAC: loss function is reprojection error
%   parameters are 
%         loss function
%         threshold
%         iterations
% 
%   Integration between fmatrix() and ransac()
%       fmatrix.ransac_driver is the function that operates within and e.g. sets sample size=8
%       
% fmatrix()
%   conditioning of data: normalise to zero mean and standard deviation = sqrt(2): uses
%       vgg_conditioner_from_pts, vgg_condition_2d
% 
% inliers:  [bestInliers, bestF] = funddist(F, x, t)  does give inliers, but ransac(@fmatrix,...) does not
% inliers are updated in the matches object
% 
% usage: [Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose','maxTrials',1e4)
% 
%
% NOT process for VOS - that is based on Lie algebra, with known baseline to get scale
% 
% See Robotics, Vision and Control pp391-394 14.2.3 Estimating the Fundamental Matrix - SURF etc - 
%     F = m.ransac(@fmatrix, 1e-4, 'verbose')
%   Fundamental Matrix defn and equations - pp388-9
%   Essential Matrix defn and equations - pp390-1
%       From Fundamental Matrix - cam1.invE(E)
%       check by reprojection of point in front of camera - Q = [0 0 10]'; cam1.move(sol(:,:,1)).project(Q)
%           implemented as sol = cam1.invE(E, Q)

% isurf 
%       The SurfPointFeature object has many properties including:
%        u            horizontal coordinate
%        v            vertical coordinate
%        strength     feature strength
%        descriptor   feature descriptor (64x1 or 128x1)
%        sigma        feature scale
%        theta        feature orientation [rad]
% 
%       Options::
%       'nfeat',N      set the number of features to return (default Inf)
%       'thresh',T     set Hessian threshold.  Increasing the threshold reduces
%                      the number of features computed and reduces computation time.
%       'octaves',N    number of octaves to process (default 5)
%       'extended'     return 128-element descriptor (default 64)
%       'upright'      don't compute rotation invariance
%       'suppress',R   set the suppression radius (default 0).  Features are not
%                      returned if they are within R [pixels] of an earlier (stronger)
%                      feature.
% 
% 
% 
% 
% See https://au.mathworks.com/help/vision/examples/uncalibrated-stereo-image-rectification.html
%       RANSAC	RANdom SAmple Consensus. Select this method if you would like to set the distance threshold for the inliers.
%       MSAC	M-estimator SAmple Consensus. Select the M-estimator SAmple Consensus method if you would like to set the distance threshold for the inliers. Generally, the MSAC method converges more quickly than the RANSAC method.
%         'DistanceType' � Algebraic or Sampson distance type
%             'Sampson' (default) | 'Algebraic'
%         'DistanceThreshold' � Distance threshold for finding outliers
%             0.01 (default)
%         'ReportRuntimeError' � Report runtime error
%             true (default) | false
%         status � Status code
%             0 no error | 1 not enough points | 2 not enough inliers found
%         uses the 8-point algorithm:
%           http://www.cs.unc.edu/~marc/tutorial/node54.html
%           https://en.wikipedia.org/wiki/Eight-point_algorithm
%           http://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/f07/proj_final/www/amichals/fundamental.pdf


!!! https://au.mathworks.com/help/vision/ref/relativecamerapose.html !!!
https://au.mathworks.com/help/vision/examples/structure-from-motion-from-two-views.html
https://au.mathworks.com/help/vision/examples/structure-from-motion-from-multiple-views.html


http://openmvg.readthedocs.io/en/latest/openMVG/multiview/multiview/
http://openmvg.readthedocs.io/en/latest/openMVG/robust_estimation/robust_estimation/
http://openmvg.readthedocs.io/en/latest/openMVG/matching/matching/
http://openmvg.readthedocs.io/en/latest/openMVG/tracks/tracks/
http://openmvg.readthedocs.io/en/latest/openMVG/geometry/geometry/
http://www.icg.tugraz.at/publications/inproceedings.2010-05-25.6505863218
https://www.researchgate.net/publication/274094180_Factorization_of_View-Object_Manifolds_for_Joint_Object_Recognition_and_Pose_Estimation



https://au.mathworks.com/help/vision/ref/relativecamerapose.html

http://iitlab.bit.edu.cn/mcislab/~yangjiaolong/yang_et_al_eccv14_optimal_ematrix.pdf
Optimal Essential Matrix Estimation via Inlier-Set Maximization

https://en.wikipedia.org/wiki/Random_sample_consensus



















