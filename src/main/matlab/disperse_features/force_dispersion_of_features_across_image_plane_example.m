% Assumes that sampling of points in all dimensions of a 3D scene be improved by sampling 
% from all parts of the image plane, while still rejecting weak features
% within each part of the image sampled.
% Intuitively include more weak features than top-n features across the
% whole image; latter is global, this is local. 

I = imread('cameraman.tif');
points = detectSURFFeatures(I,'MetricThreshold',100); % default 'MetricThreshold' is 1000.0


figure('Name','strongest by plain strength');
imshow(I); hold on;
plot(points.selectStrongest(80));

figure('Name','strongest by image region : 4 regions');
imshow(I); hold on;
a = [points.Location];
isTopLeft=a(:,1)<128 & a(:,2)<128;
isTopLeft=a(:,1)<128 & a(:,2)<128;
isTopRight=a(:,1)>=128 & a(:,2)<128;
isBottomLeft=a(:,1)<128 & a(:,2)>=128;
isBottomRight=a(:,1)>=128 & a(:,2)>=128;
sum(isBottomLeft) + sum(isBottomRight) + sum(isTopLeft) + sum(isTopRight)
subsetOfPoints = points(isTopLeft); plot(subsetOfPoints.selectStrongest(20));
subsetOfPoints = points(isTopRight); plot(subsetOfPoints.selectStrongest(20));
subsetOfPoints = points(isBottomLeft); plot(subsetOfPoints.selectStrongest(20));
subsetOfPoints = points(isBottomRight); plot(subsetOfPoints.selectStrongest(20));

%------ compare to this function ----
figure('Name','selectUniform');
imshow(I); hold on;
uniformlySelectedPoints = selectUniform(points, 80, size(I));
plot(uniformlySelectedPoints);


%  See stongest_by_region
figure('Name','strongest by image region : 6x6 regions');
imshow(I); hold on;
a = [points.Location];
for u_ = 1:6
    for v_ = 1:6
        left_   = ((u_-1)*(size(I,2)/6)) + 1;
        right_  = ((u_)*(size(I,2)/6)) + 1;
        up_     = ((v_)*(size(I,1)/6)) + 1;
        down_   = ((v_-1)*(size(I,1)/6)) + 1;
        isIn= a(:,1)>=left_ & a(:,1)<right_ & a(:,2)>=down_ & a(:,2)<up_;
        if sum(isIn) > 0
            subsetOfPoints = points(isIn); 
            plot(subsetOfPoints.selectStrongest( 3 ));
        end;
    end;
end;


figure('Name','stongest_by_region(points, size(I), 6, 6, 3)');
imshow(I); hold on;
strong_points = stongest_by_region(points, size(I), 6, 6, 3);
plot(strong_points);

fig_handle = gcf;                       % Get Current Figure
fig_handle.InvertHardcopy = 'off';      % hard copy = printable ;  'on' ==> change the background colour to white 
print('D:\_will\owncloud\project_AA1__1_1\disperse_features\stongest_by_region__plottedWithoutUIControls','-dpng','-noui');  % filename==> save as file , -d ==> png , 'no UI'
print('D:\_will\owncloud\project_AA1__1_1\disperse_features\stongest_by_region__plottedWithoutUIControls','-dpdf','-noui');
