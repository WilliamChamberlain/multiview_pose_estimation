 function points_ = stongest_by_region(feature_points_, image_size_, num_regions_horz_, num_regions_vert_, num_per_region_)
    pt_locs_in_image = [feature_points_.Location];
    points_ = SURFPoints
    for u_ = 1:num_regions_horz_
        for v_ = 1:num_regions_vert_
            left_   = ((u_-1)*(image_size_(2)/num_regions_horz_)) + 1;
            right_  = ((u_)*(image_size_(2)/num_regions_horz_)) + 1;
            up_     = ((v_)*(image_size_(1)/num_regions_vert_)) + 1;
            down_   = ((v_-1)*(image_size_(1)/num_regions_vert_)) + 1;
            isIn = pt_locs_in_image(:,1)>=left_ & pt_locs_in_image(:,1)<right_ & pt_locs_in_image(:,2)>=down_ & pt_locs_in_image(:,2)<up_;
            if sum(isIn) > 0
                subsetOfPoints = feature_points_(isIn); 
                points_ = [ points_ ; subsetOfPoints.selectStrongest( num_per_region_ ) ] ;
            end;
        end;
    end;
 end