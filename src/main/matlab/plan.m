Tried setting a minimum scale

try matching the image scales - would need to match something in there first to get the scale
    
try eliminating lower-level details - note that this is what SIFT/SURF already do

estimate the pose from fundamental 
    - in robot x-y-z frame : 
    CCTV z ~ 175cm - robot z/up ~ 25cm 
    and x/right ~ 40cm 
    and y/forward ~ 30cm

try to finic the homography into it - Viorella's pose graph uses homographies

finic MSER into it - 
combine SIFT/SURF and MSER
    --> new feature ! 

Vincent - ORB and BRIEF for speed
Vincent - not enough of matches 
    me --> need larger scale, less discriminative

real-time

Keep an eye on the target - estimation against a dynamic environment  
    --> MSER etc may not apply