
f_handle_1_ = figure('Name','threshold sweep');

[im_1,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_5000.jpg');
[im_2,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_4000.jpg');


threshold__     = zeros(20*5,1,'double');
rpy_            = zeros(20*5,3,'double');
translation     = zeros(3,20*5,'double');
residual_       = zeros(20*5,1,'double');
elapsedTime_    = zeros(20*5,1,'double');
SurfPointFeature_size_ = zeros(20*5,2,'uint32');

iteration_ = 0;
% for i_ = 0.0008:0.01:0.0208
for i_ = 0.0008:0.0040:0.0208
    iteration_=iteration_+1;
    threshold_=i_;
    display(sprintf('threshold_=%d',threshold_));
    f_handle_ = figure('Name',sprintf('threshold sweep %d',threshold_));
    tic
    [SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] ...
        = surf_detect_match_display_with_subimages_and_border( f_handle_, ...
            im_1, im_2, ... 
            1, 10000, 30, ...  
            threshold_);
    elapsedTime__ = toc;
        for j_ = 1:5
            threshold__(iteration_+j_) = threshold_;
            elapsedTime_(iteration_+j_) = elapsedTime__;
            SurfPointFeature_size_(iteration_+j_,1) = length(SurfPointFeature_1);
            SurfPointFeature_size_(iteration_+j_,2) = length(SurfPointFeature_2);
            if size(SurfPointFeature_1,2) <=0 || size(SurfPointFeature_2,2) <=0  
                residual_(iteration_+j_)        = NaN;
                rpy_(iteration_+j_,:)           = NaN;
                translation_(:,iteration_+j_)   = NaN;
                continue;
            end;
            try
                [Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose','maxTrials',1e5,'maxDataTrials',1e4);
                residual_(iteration_+j_) = residual;
            
                camera__1 = CentralCamera( ...
                    'name','camera__1',  ...    
                    'image', im_1, ...
                    'focal', mean(tags_1.DigitalCamera.FocalLength * 1e-3), ...    
                    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
%                 figure('Name','Camera 1 epilines of camera 2 inliers');
%                 camera__1.plot_epiline(Fund_model',matches.inlier.p2,'g');
%                 camera__1.plot_epiline(Fund_model',matches.inlier.subset(30).p2,'g');

                camera__2 = CentralCamera( ...
                    'name','camera__2',  ...    
                    'image', im_2, ...
                    'focal', mean(tags_2.DigitalCamera.FocalLength * 1e-3), ...    
                    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
%                 figure('Name','Camera 2 epilines of camera 1 inliers_');
%                 camera__2.plot_epiline(Fund_model,matches.inlier.p1,'g');

                E = camera__1.E(Fund_model);
                sol = camera__1.invE(E, [0,0,10]');
                [R,t] = tr2rt(sol);
                t = 1 * t/t(1);                           %  !  prior - 0.2cm between photos
                T = rt2tr(R, t);
                [R,t] = tr2rt(T);
                rpy_(iteration_+j_,:) = tr2rpy(R, 'deg');
                translation_(:,iteration_+j_) = t;
            catch MException
                residual_(iteration_+j_)        = NaN;
                rpy_(iteration_+j_,:)           = NaN;
                translation_(:,iteration_+j_)   = NaN;
                display(sprintf('Exception with thresh=%d',threshold_));
                display(MException.message);
            end;            
            
        end;
end