%{
Calibrated, registered fixed cameras.
Robot with calibrated camera.
Robot picks features in field of view, pushes to network.
Cameras match features, push back pose+rays+error_estimates /or/
pose+feature_positions+error_estimates.
    May include multiple matches if the feature is not discriminative enough.
Robot constructs and traverses graph. 
%}


% detect_and_draw_sift_features(image_, figure_handle_,edge_threshold_::10,peak_threshold_)

% base_dir = 'H:\Documents\ownCloud'
base_dir = 'D:\_will\ownCloud'

vl_feat_edge_threshold_default=10;
vl_feat_peak_threshold_default=0;

for edge_threshold_ = 10:-1:1 
    im = imread(fullfile(base_dir,'\project_AA1__1_1','robot_3_pose_1 (1).jpg'));
    figHandle_robot_3_pose_1_1=figure('Name', sprintf('robot_3_pose_1_1 SIFT edge = %d',edge_threshold_)); 
    imshow(im);
    robot_3_pose_1_1 = im; 
    [features_,descriptors_] = detect_and_draw_sift_features(robot_3_pose_1_1, figHandle_robot_3_pose_1_1,edge_threshold_,vl_feat_peak_threshold_default);
end


% for peak_threshold_ = 0:10:100 
%     im = imread(fullfile(base_dir,'\project_AA1__1_1','robot_3_pose_1 (1).jpg'));
%     figHandle_robot_3_pose_1_1=figure('Name', sprintf('robot_3_pose_1_1 SIFT peak = %d',peak_threshold_)); 
%     imshow(im);
%     robot_3_pose_1_1 = im; 
%     [features_,descriptors_] = detect_and_draw_sift_features(robot_3_pose_1_1, figHandle_robot_3_pose_1_1,vl_feat_edge_threshold_default,peak_threshold_);
% end


for peak_threshold_ = 0:2:10 
    im = imread(fullfile(base_dir,'\project_AA1__1_1','robot_3_pose_1 (1).jpg'));
    figHandle_robot_3_pose_1_1=figure('Name', sprintf('robot_3_pose_1_1 SIFT peak = %d',peak_threshold_)); 
    imshow(im);
    robot_3_pose_1_1 = im; 
    [features_,descriptors_] = detect_and_draw_sift_features(robot_3_pose_1_1, figHandle_robot_3_pose_1_1,vl_feat_edge_threshold_default,peak_threshold_);
end

% im_camera_4_pose_1 = imread(fullfile(base_dir,'\project_AA1__1_1','camera_4_pose_1 (1).jpg'));
im_camera_1_pose_1 = imread(fullfile(base_dir,'\project_AA1__1_1','camera_1_pose_1 (1).jpg'));
im_robot_3_pose_1 = imread(fullfile(base_dir,'\project_AA1__1_1','robot_3_pose_1 (1).jpg'));

% im_camera_4_pose_1 = single(rgb2gray(im_camera_4_pose_1));      % pre-convert; convert in vlfeat_sift_mosaic doesn't respond well to peak_threshold
im_camera_1_pose_1 = single(rgb2gray(im_camera_1_pose_1));      % pre-convert; convert in vlfeat_sift_mosaic doesn't respond well to peak_threshold
im_robot_3_pose_1  = single(rgb2gray(im_robot_3_pose_1));      % pre-convert; convert in vlfeat_sift_mosaic doesn't respond well to peak_threshold

vlfeat_sift_mosaic_peakthreshold(im_camera_1_pose_1,im_robot_3_pose_1,10,5);

vlfeat_sift_mosaic_peakthreshold(im_camera_1_pose_1,im_robot_3_pose_1,40,0.0025);

