% isnan(not_observed())

function pixels_ = not_observed() 
    pixels_ = NaN(1,2,'double');
end