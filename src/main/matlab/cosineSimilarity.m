function cosTheta = cosineSimilarity(u,v)
    dot_uv = dot(u,v);
    norm_product = (norm(u)*norm(v));
    if 0 == sum(dot_uv) && 0 == sum(norm_product)
        cosTheta = 0;
    else
        cosTheta = dot(u,v) ./ ( sqrt(sum(u.^2,1)) .*  sqrt(sum(v.^2,1)) ) ;   % norm norms the whole matrix rather than by column or by row, and norm(x) = norm(x,2)
    end
end