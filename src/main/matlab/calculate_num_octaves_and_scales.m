function [num_octaves, num_scales] = calculate_num_octaves_and_scales(image_)
% Calculate how many octaves and scales to use: currently fixed to [ 3 , 4
% ] to match Matlab's detectSURFFeatures defaults
    num_octaves = 3;
    num_scales = 4;
end