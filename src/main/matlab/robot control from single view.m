robot control from single view

closed loop control of the robot from single camera view : 
    given essential matrix 
    and assuming the robot height for scale, 
    control the robot through a set of waypoints
        for e.g. exploration
                
            
aim : RSS : 
    need a working system doing something interesting           
    conv nice to have to demonstrate new use for conv
    conv nice to have because useful feature of other tasks and core tech of online & business services is a useful component of robot system

    
planning :    
    RSS submission mid-end Jan
    need main argument , outline of results , experiment by mid-November

%---------------------------------------

- statistics come out in the match if you use the RVC match rather than through the SurfFeatures.match

- Use Computer Visison toolbox implementation of SURF: much faster - Peter's will do that by default in the future

- use 'median' as a rough adaptive threshold for SURF feature detection - current default is a fixed threshold

- strength-based suppression / nearest-neighbour algorithm is arbitrary, e.g. 9-8-7 neighbours with the current typical fixed-radius neighbourhood: 9 suppresses 8, 7 is then not suppressed.
    Could equally well use a suppression function which includes a weighting of the point strength, distance (e.g. flat to radius*0.9, then fall off) 
    Infinite variety, but hard to see a way through to a science for choosing
        Depends on the scene structure - planar objects different from convex/cave objects, etc.
            Natural objects different from artificial - textured vs smooth
            
%---------------------------------------

poll cameras            -> im_1
poll robot (camera)     -> im_2   % note: Assign Code to Cores - https://au.mathworks.com/help/simulink/ug/concurrent-execution-example.html
estimate pose up to scale
use robot camera height = Z ==> scale
points on floor - if visible - to derive plane ==> robot camera Z
have robot camera pose ==> robot pose 
path plan 
navigate
    
do it all again in a dynamic environment




