% Robotics, Vision and Control p400-402

% Estimate the variance of the relative camera pose 
% - from the essential matrix estimation - 
% with the residual.

im1=iread('garden-l.jpg','double');
im2=iread('garden-r.jpg','double');  

idisp({im1,im2})

s1 = isurf(im1);
s2 = isurf(im2);
m = s1.match(s2);
m.show()  
% Ubuntu 14.04 64-bit:
% ans =
% 132 corresponding points
% Book lists "323 corresponding points"
m = s1.match(s2,'median');
m.show
% ans =
% 165 corresponding points

% looks like a good correspondence set!
m.plot()

[H,r] = m.ransac(@homography,2)
m.show
% ans =
% 165 corresponding points
% 52 inliers (31.5%)
% 113 outliers (68.5%)

% points on the wall, the plants in the pots, the bush growing up against
% the wall - only approximately planar/homography
idisp({im1,im2})
m.inlier.plot() 

m2=m.outlier

[H2,r2] = m2.ransac(@homography,2)
m2.show
% ans =
% 113 corresponding points
% 24 inliers (21.2%)
% 89 outliers (78.8%)


% points on the bush growing up against the wall, the exhaust vents, 
% the wall, the plants in the pots,  - only approximately planar/homography

idisp({im1,im2})
m2.inlier.plot() 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = s1.match(s2,'median')
%165 NOT 323 

[im1, tags] = iread('garden-l.jpg','double','mono');
tags.DigitalCamera
cam = CentralCamera('image',im1,'focal',5.2e-3,'sensor',[7.18e-3,5.32e-3])
[F,r] = m.ransac(@fmatrix,1e-4,'verbose');
cam.plot_epiline(F', m.inlier.subset(30).p2, 'r');
    
for i_ = 1:100
    try
        [F,r] = m.ransac(@fmatrix,1e-4);
        fund_(:,:,i_) = F;
        r_(i_) = r;
        E = cam.E(F);
        E_(:,:,i_) = E;
        sol = cam.invE(E,[0,0,10]');
        sol_(:,:,i_) = sol;
        [R,t] = tr2rt(sol);
        rpy=tr2rpy(R,'deg');
        rpy_(:,:,i_) = rpy;
        t = 0.3 * t/t(1);
        T = rt2tr(R,t);
        T_(:,:,i_) = T;
    catch 
        display(sprintf('Error at iteration %d; continuing.',i_));
    end
end;


