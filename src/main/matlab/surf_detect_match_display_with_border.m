%{
im_ = imread('D:\_will\owncloud\project_AA1__1_1\robot_3_pose_1 (4).jpg');
im_2 = imread('D:\_will\owncloud\project_AA1__1_1\robot_2_pose_1 (1).jpg');
f_handle_ = figure('Name','im_ im_2')
[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages(f_handle_, im_, im_2, 100);
[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages(f_handle_, im_, im_2, 50);
%}


function [SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_border(figure_handle_, im1, im2, features_per_region_, neighbourhood_px_, border_px_, varargin)
% SurfPointFeature.match :
%   parameters are applied equally to each image
% 
% Requires Peter Corke's Machine Vision Toolbox
%
% Detection and description: OpenSurf.m
% Options::
% 'nfeat',N      set the number of features to return (default Inf)
% 'thresh',T     set Hessian threshold.  Increasing the threshold reduces
%                the number of features computed and reduces computation time (default 0.0008).
% 'octaves',N    number of octaves to process (default 5)
% 'extended'     return 128-element descriptor (default 64)
% 'upright'      don't compute rotation invariance
% 'suppress',R   set the suppression radius (default 0).  Features are not
%                returned if they are within R [pixels] of an earlier (stronger)
%                feature.
% defaultoptions=struct('tresh',0.0008,'octaves',5,'init_sample',2,'upright',false,'extended',false,'verbose',false);
%
% Matching: SurfPointFeature.match
% Options::
%   'thresh',T    Match threshold (default 0.05)
%   'median'      Threshold at the median distance
% 

% Adapted to SURF from VLFeat code:
% SIFT_MOSAIC Demonstrates matching two images using SIFT and RANSAC
%

% make grayscale
im1g = imono(im1);
im2g = imono(im2);

figure(figure_handle_);
hold off;
% idisp({im1,im2},'plain')
idisp({im1,im2});
drawnow;
% imshow([im1,im2])
hold on;

% --------------------------------------------------------------------
%                                                         SIFT matches
% --------------------------------------------------------------------


SurfPointFeature_1 = [];
SurfPointFeature_2 = [];

    image_size = size(im1g);
    image_size = image_size - [border_px_*2 , border_px_*2];
    vertical_divs=1;
    horizontal_divs=1;
    overlap_px = 20;
    sub_image_height=ceil(image_size(1)/horizontal_divs);
    sub_image_width=ceil(image_size(2)/horizontal_divs);
    vertical_offset_px   = ceil(sub_image_height/2);
    horizontal_offset_px = ceil(sub_image_width/2);
    
    opt.colour='g';
    opt = tb_optparse(opt, varargin);
    
    if opt.colour 
        colour_ = opt.colour;
    else 
        colour_ = 'g';
    end
        
    function SurfPointFeature_1_sub = isurf_with_offset(img_, offset_vert, offset_horz, varargin) 
        if neighbourhood_px_ > 0
            SurfPointFeature_1_sub = isurf(img_(sub_image_top:sub_image_bottom,sub_image_left:sub_image_right), 'nfeat', features_per_region_, 'suppress', neighbourhood_px_, varargin);            
        elseif features_per_region_ > 0
            SurfPointFeature_1_sub = isurf(img_(sub_image_top:sub_image_bottom,sub_image_left:sub_image_right), 'nfeat', features_per_region_, varargin);            
        else
            SurfPointFeature_1_sub = isurf(img_(sub_image_top:sub_image_bottom,sub_image_left:sub_image_right), varargin);            
        end;
        for elem_num = 1:length(SurfPointFeature_1_sub)
            SurfPointFeature_1_sub(elem_num).offset_v(offset_vert-1);
            SurfPointFeature_1_sub(elem_num).offset_u(offset_horz-1); 
        end;
    end;

            sub_image_top = border_px_; 
            sub_image_bottom = image_size(1);
            sub_image_left = border_px_;
            sub_image_right = image_size(2);
            
%             if 0 < length(SurfPointFeature_1_sub), SurfPointFeature_1 = horzcat(SurfPointFeature_1,SurfPointFeature_1_sub); end;
            
            SurfPointFeature_1 = horzcat(SurfPointFeature_1,isurf_with_offset(im1g, sub_image_top-1,sub_image_left-1, varargin));
            SurfPointFeature_2 = horzcat(SurfPointFeature_2,isurf_with_offset(im2g, sub_image_top-1,sub_image_left-1, varargin));
            
%             display(sprintf('found %d features for subimage t=%d b=%d l=%d r=%d', length(SurfPointFeature_1_sub), sub_image_top, sub_image_bottom, sub_image_left, sub_image_right));

            hold on 
            plot([sub_image_left sub_image_right sub_image_right sub_image_left sub_image_left] , ...
                [sub_image_top sub_image_top sub_image_bottom sub_image_bottom sub_image_top], colour_);
            plot([sub_image_left sub_image_right sub_image_right sub_image_left sub_image_left]+size(im1g,2) , ...
                [sub_image_top sub_image_top sub_image_bottom sub_image_bottom sub_image_top], colour_);
            drawnow;
            
    display(sprintf('Found %d features in first image.',length(SurfPointFeature_1)));
    
    
%     
%     for vertical_div = 1:vertical_divs-1
%         if vertical_div == 0
%             sub_image_top = 1; %vertical_div*sub_image_height;
%         else
%             sub_image_top = vertical_div*sub_image_height - overlap_px - vertical_offset_px;
%         end
%         if vertical_div < vertical_divs-1
%             sub_image_bottom = (vertical_div+1)*sub_image_height + overlap_px - vertical_offset_px;
%         else
%             sub_image_bottom = image_size(1) - vertical_offset_px;
%         end
%         for horizontal_div = 1:horizontal_divs-1
%             if horizontal_div > 0 
%                 sub_image_left = horizontal_div*sub_image_width - overlap_px - horizontal_offset_px;
%             else
%                 sub_image_left = 1; %horizontal_div*sub_image_width;
%             end
%             if horizontal_div < horizontal_divs-1
%                 sub_image_right = (horizontal_div+1)*sub_image_width + overlap_px - horizontal_offset_px;
%             else
%                 sub_image_right = image_size(2) - horizontal_offset_px;
%             end
%             
%             SurfPointFeature_1_sub = isurf(im1g(sub_image_top:sub_image_bottom,sub_image_left:sub_image_right), 'nfeat', features_per_region_);
%             for elemNum = 1:length(SurfPointFeature_1_sub)
%                 SurfPointFeature_1_sub(elemNum).offset_v(sub_image_top-1);
%                 SurfPointFeature_1_sub(elemNum).offset_u(sub_image_left-1); 
%             end;
%             if 0 < length(SurfPointFeature_1_sub), SurfPointFeature_1 = horzcat(SurfPointFeature_1,SurfPointFeature_1_sub); end;
%             display(sprintf('found %d features for subimage t=%d b=%d l=%d r=%d', length(SurfPointFeature_1_sub), sub_image_top, sub_image_bottom, sub_image_left, sub_image_right));
% 
%             hold on 
%             plot([sub_image_left sub_image_right sub_image_right sub_image_left sub_image_left] , ...
%                 [sub_image_top sub_image_top sub_image_bottom sub_image_bottom sub_image_top], colour_);
%             drawnow;
%         end        
%     end
%     display(sprintf('Found %d features in first image.',length(SurfPointFeature_1)));
    
    % features in the first image can match features anywhere in the second
    % image.
%     SurfPointFeature_2 = isurf(im2g); 
    display(sprintf('Found %d features in second image.',length(SurfPointFeature_2)));

[matches, correspondences] = SurfPointFeature_1.match(SurfPointFeature_2);

matches.plot()
% 
% numMatches = size(matches,1) ;
% 
% 
% % --------------------------------------------------------------------
% %  can't use RANSAC with homography model as VLFeat demo does; theirs
% %  assumes translation and rotation around the camera origin, not around
% %  the scene.
% %  Need to RANSAC over the reprojection error.
% % --------------------------------------------------------------------
% % % 
% % X1 = f1(1:2,matches(1,:)) ; X1(3,:) = 1 ;
% % X2 = f2(1:2,matches(2,:)) ; X2(3,:) = 1 ;
% % clear H score ok ;
% % for t = 1:100
% %   % estimate homograpyh
% %   subset = vl_colsubset(1:numMatches, 4) ;
% %   A = [] ;
% %   for i = subset
% %     A = cat(1, A, kron(X1(:,i)', vl_hat(X2(:,i)))) ;
% %   end
% %   [U,S,V] = svd(A) ;
% %   H{t} = reshape(V(:,9),3,3) ;
% % 
% %   % score homography
% %   X2_ = H{t} * X1 ;
% %   du = X2_(1,:)./X2_(3,:) - X2(1,:)./X2(3,:) ;
% %   dv = X2_(2,:)./X2_(3,:) - X2(2,:)./X2(3,:) ;
% %   ok{t} = (du.*du + dv.*dv) < 6*6 ;
% %   score(t) = sum(ok{t}) ;
% % end
% % 
% % [score, best] = max(score) ;
% % H = H{best} ;
% % ok = ok{best} ;
% % 
% % % --------------------------------------------------------------------
% % %                                                  Optional refinement
% % % --------------------------------------------------------------------
% % 
% % function err = residual(H)
% %  u = H(1) * X1(1,ok) + H(4) * X1(2,ok) + H(7) ;
% %  v = H(2) * X1(1,ok) + H(5) * X1(2,ok) + H(8) ;
% %  d = H(3) * X1(1,ok) + H(6) * X1(2,ok) + 1 ;
% %  du = X2(1,ok) - u ./ d ;
% %  dv = X2(2,ok) - v ./ d ;
% %  err = sum(du.*du + dv.*dv) ;
% % end
% % 
% % if exist('fminsearch') == 2
% %   H = H / H(3,3) ;
% %   opts = optimset('Display', 'none', 'TolFun', 1e-8, 'TolX', 1e-8) ;
% %   H(1:8) = fminsearch(@residual, H(1:8)', opts) ;
% % else
% %   warning('Refinement disabled as fminsearch was not found.') ;
% % end
% 
% % --------------------------------------------------------------------
% %                                                         Show matches
% % --------------------------------------------------------------------
% 
% dh1 = max(size(im2,1)-size(im1,1),0) ;
% dh2 = max(size(im1,1)-size(im2,1),0) ;
% 
% figure(1) ; clf ;
% subplot(2,1,1) ;
% imagesc([padarray(im1,dh1,'post') padarray(im2,dh2,'post')]) ;
% o = size(im1,2) ;
% line()
% line([f1(1,matches(1,:));f2(1,matches(2,:))+o], ...
%      [f1(2,matches(1,:));f2(2,matches(2,:))]) ;
% title(sprintf('%d tentative matches', numMatches)) ;
% axis image off ;
% 
% subplot(2,1,2) ;
% imagesc([padarray(im1,dh1,'post') padarray(im2,dh2,'post')]) ;
% o = size(im1,2) ;
% line([f1(1,matches(1,ok));f2(1,matches(2,ok))+o], ...
%      [f1(2,matches(1,ok));f2(2,matches(2,ok))]) ;
% title(sprintf('%d (%.2f%%) inliner matches out of %d', ...
%               sum(ok), ...
%               100*sum(ok)/numMatches, ...
%               numMatches)) ;
% axis image off ;
% 
% drawnow ;
% 
% % --------------------------------------------------------------------
% %                                                               Mosaic
% % --------------------------------------------------------------------
% 
% box2 = [1  size(im2,2) size(im2,2)  1 ;
%         1  1           size(im2,1)  size(im2,1) ;
%         1  1           1            1 ] ;
% box2_ = inv(H) * box2 ;
% box2_(1,:) = box2_(1,:) ./ box2_(3,:) ;
% box2_(2,:) = box2_(2,:) ./ box2_(3,:) ;
% ur = min([1 box2_(1,:)]):max([size(im1,2) box2_(1,:)]) ;
% vr = min([1 box2_(2,:)]):max([size(im1,1) box2_(2,:)]) ;
% 
% [u,v] = meshgrid(ur,vr) ;
% im1_ = vl_imwbackward(im2double(im1),u,v) ;
% 
% z_ = H(3,1) * u + H(3,2) * v + H(3,3) ;
% u_ = (H(1,1) * u + H(1,2) * v + H(1,3)) ./ z_ ;
% v_ = (H(2,1) * u + H(2,2) * v + H(2,3)) ./ z_ ;
% im2_ = vl_imwbackward(im2double(im2),u_,v_) ;
% 
% mass = ~isnan(im1_) + ~isnan(im2_) ;
% im1_(isnan(im1_)) = 0 ;
% im2_(isnan(im2_)) = 0 ;
% mosaic = (im1_ + im2_) ./ mass ;
% 
% figure(2) ; clf ;
% imagesc(mosaic) ; axis image off ;
% title('Mosaic') ;
% 
% if nargout == 0, clear mosaic ; end

end