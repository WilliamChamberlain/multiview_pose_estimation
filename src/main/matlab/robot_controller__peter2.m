robot_controller_addpath()

do_dstar_ = false;
do_animate_translation_from_pose_to_pose_ = false;

target_pose_world = [1,1,1];  % - want to drive it into the far left corner for some reason

target_pose_relative_xy = [0,0]; % - want to move it vertically under the camera for some reason

target_orientation_relative = rpy2r([0,0,0]);
target_location_relative = [0,0,0]';
target_pose_relative = rt2tr(target_orientation_relative,target_location_relative);

% [robot_image,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_5000.jpg', 'reduce', 2);
% [camera_image,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_1000.jpg', 'reduce', 2);
% [camera_image,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_09_07\camera_1_pose_1 (1).jpg');
% % [robot_image,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_09_07\robot_1_pose_7 (3).jpg');
% [robot_image,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_2000.jpg', 'reduce', 2);
% [camera_image,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_1000.jpg', 'reduce', 2);
% [robot_image,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_2000.jpg');
% 
% [robot_image,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_3000.jpg', 'reduce', 2);
% [camera_image,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\Phase_1_pics_2016_10_18\S11_lab_1000.jpg', 'reduce', 2);

[robot_image,tags_1] = iread(fullfile('../../../../../Phase_1_pics_2016_10_18/S11_lab_3000.jpg'), 'reduce', 2);
[camera_image,tags_2] = iread(fullfile('../../../../../Phase_1_pics_2016_10_18/S11_lab_1000.jpg'), 'reduce', 2);

known_separation_x = 2.00;

ransac_threshold__     = zeros(12*5,1,'double');
rpy_            = zeros(12*5,3,'double');
translation_     = zeros(3,12*5,'double');
residual_       = zeros(12*5,1,'double');'
elapsedTime_    = zeros(12*5,1,'double');
SurfPointFeature_size_ = zeros(12*5,2,'uint32');
ransac_stats_   = zeros(12*5,3,'double');

iteration_ = 0;
% for i_ = 0.0008:0.01:0.0208
f_handle_ = figure('Name',sprintf('estimating pose for planning iteration %d',iteration_));
 [SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] ...
     = surf_detect_match_display_with_subimages_and_border( f_handle_, ...
         robot_image, camera_image, ... 
         2, 100, 30, ...          % NOTE : increase border to reduce computation
         0.0016);
%          1, 10000, 30, ...          % NOTE : increase border to reduce computation
%          0.0016);
%          0.0008);
    
    ransac_threshold_=1.0000e-05;
    display(sprintf('ransac_threshold_=%d',ransac_threshold_));
    
        for j_ = 1:5            
            if size(SurfPointFeature_1,2) <=0 || size(SurfPointFeature_2,2) <=0  
                residual_(iteration_+j_)        = NaN;
                rpy_(iteration_+j_,:)           = NaN;
                translation_(:,iteration_+j_)   = NaN;
                continue;
            end;
            iteration_=iteration_+1;
            display(sprintf('estimating pose, iteration %d',iteration_));
            ransac_threshold__(iteration_+j_) = ransac_threshold_;
            SurfPointFeature_size_(iteration_+j_,1) = length(SurfPointFeature_1);
            SurfPointFeature_size_(iteration_+j_,2) = length(SurfPointFeature_2);
            try                
                start_time_ = tic;
                [Fund_model, residual, ransac_stats] = matches.ransac(@fmatrix, ransac_threshold_, 'verbose','maxTrials',2e4,'maxDataTrials',1e3);  % TODO - 
                display('fitted model with RANSAC');            
                toc(start_time_)
                ransac_stats_(iteration_+j_,1) = ransac_stats(1);
                ransac_stats_(iteration_+j_,2) = ransac_stats(2);
                ransac_stats_(iteration_+j_,3) = ransac_stats(3);
                residual_(iteration_+j_) = residual;
            
                camera__1 = CentralCamera( ...
                    'name','camera__1',  ...    
                    'image', robot_image, ...
                    'focal', mean(tags_1.DigitalCamera.FocalLength * 1e-3), ...    
                    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
%                 figure('Name','Camera 1 epilines of camera 2 inliers');
%                 camera__1.plot_epiline(Fund_model',matches.inlier.p2,'g');
%                 camera__1.plot_epiline(Fund_model',matches.inlier.subset(30).p2,'g');

                camera__2 = CentralCamera( ...
                    'name','camera__2',  ...    
                    'image', camera_image, ...
                    'focal', mean(tags_2.DigitalCamera.FocalLength * 1e-3), ...    
                    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
%                 figure('Name','Camera 2 epilines of camera 1 inliers_');
%                 camera__2.plot_epiline(Fund_model,matches.inlier.p1,'g');

                E = camera__1.E(Fund_model);
                sol = camera__1.invE(E, [0,0,10]');
                [R,t] = tr2rt(sol);
                t = known_separation_x * t/t(1);                           %  !  prior - 1m between photos
                T = rt2tr(R, t);
                [R,t] = tr2rt(T);
                rpy_(iteration_+j_,:) = tr2rpy(R, 'deg');
                translation_(:,iteration_+j_) = t;              
                display(sprintf('finished iteration %d',iteration_));
                elapsedTime_(iteration_+j_) = toc(start_time_);
            catch MException
                residual_(iteration_+j_)        = NaN;
                rpy_(iteration_+j_,:)           = NaN;
                translation_(:,iteration_+j_)   = NaN;
                display(sprintf('Exception with ransac_threshold=%d',ransac_threshold_));
                display(MException.message);
            end;              
        end
        
%         translations_found_ = translation_(sum(translation_,)
%         rpy_found_ = rpy_(translation_()

    robot_pose_translation = mean(translation_(:, ...
        ( ( 0~=translation_(1,:) & ~isnan(translation_(1,:)) ) ...
        | ( 0~=translation_(2,:) & ~isnan(translation_(2,:)) ) ...
        | ( 0~=translation_(3,:) & ~isnan(translation_(3,:)) ) ...
        )),2);
    robot_pose_rpy = mean(rpy_( ...
        ( ( 0~=translation_(1,:) & ~isnan(translation_(1,:)) ) ...
        | ( 0~=translation_(2,:) & ~isnan(translation_(2,:)) ) ...
        | ( 0~=translation_(3,:) & ~isnan(translation_(3,:)) ) ...
        ),:),1);
    robot_pose_rotation = rpy2r(robot_pose_rpy,'deg');
    robot_pose = rt2tr(robot_pose_rotation, robot_pose_translation);
    
    if do_animate_translation_from_pose_to_pose_
        %% Path planning - v1 - get a smooth trajectory

        num_steps = 50;                             % TODO - num steps from distance, or proper planning with acc,vel constraints
        ctraj(robot_pose, target_pose_relative,num_steps);   % TODO - proper planning with acc,vel constraints
        tranimate(ctraj(robot_pose, target_pose_relative,50));  % TODO - make a robot move!
    end
    
    %% Path planning - v2 - D* over a map, then smooth to a set of curves that the Nao can follow
    %
    % Pure pursuit
    % RBC pp74-75
    %
    
    if do_dstar_
    [world_coordinates, object_vertices] = world_coordinates_setup();
    coordinate_limits = [min(world_coordinates); max(world_coordinates)];
    map_to_world_transform = rt2tr(rpy2r(0,0,0),[min(world_coordinates(:,1)),min(world_coordinates(:,2)),min(world_coordinates(:,3))]');
%     
%     world_coord_limits = (map_to_world_transform) \ [coordinate_limits' ; 1 , 1 ];  % equiv but faster than inv(A)*b , apparently
%     dstar_map = zeros(ceil(sum(abs(world_coord_limits(1:2,:)),2)'));
% %     idisp(dstar_map)     
%     for obj_ = 1:size(object_vertices,1)
%         obj_in_world = (map_to_world_transform) \ [squeeze(object_vertices(obj_,:,:))' ; 1 1 1 1 ];
%         min_x=floor(min(obj_in_world(1,:)));
%         max_x=ceil(max(obj_in_world(1,:)));
%         min_y=floor(min(obj_in_world(2,:)));
%         max_y=ceil(max(obj_in_world(2,:)));
%         if sum(obj_in_world(1,:)) > 0 && sum(obj_in_world(2,:)) > 0
%             dstar_map( max([min_x 1]):max_x , max([min_y,1]):max_y )=inf;
%         end
%     end; 

%     dstar_map = zeros(ceil(sum(abs(world_coord_limits(1:2,:)),2)'));
%     dstar_map = zeros(327,543);
%     dstar_map = zeros(700,643);
dstar_map = zeros(800,800);
    figure('Name','plot objects');
    for obj_ = 1:size(object_vertices,1)
%         obj_in_world = (map_to_world_transform) \ [squeeze(object_vertices(obj_,:,:))' ; 1 1 1 1 ];
        obj_in_world = [squeeze(object_vertices(obj_,:,:))' ; 1 1 1 1 ];
        min_x=abs(floor(min(obj_in_world(1,:))));
        max_x=abs(ceil(max(obj_in_world(1,:))));
          if min_x>max_x, temp_=min_x; min_x=max_x; max_x=temp_; end
        min_y=780 - abs(floor(min(obj_in_world(2,:))));
        max_y=780 - abs(ceil(max(obj_in_world(2,:))));
          if min_y>max_y , temp_=min_y; min_y=max_y; max_y=temp_; end
%         if sum(obj_in_world(1,:)) > 0 && sum(obj_in_world(2,:)) > 0
            dstar_map( max([min_x 1]):max_x , max([min_y,1]):max_y )=inf;
            hold on;
            plot( [max([min_x 1]), max([min_x 1]), max_x, max_x] , [max([min_y,1]), max_y, max([min_y,1]), max_y]);
            plot( 0 , 0 , 'gx');
%         end
    end;
    
    submap = dstar_map(1:10:800,1:10:800);
    
    figure('Name','D* map and path'); hold on;
%      ds             = Dstar(submap, 'inflate',1);
    ds             = Dstar(submap);
     display('starting D* planning'); tic; 
     camera_location_in_world=[300,20,150];
     ds.plan([30,2]);  % goal is camera location; 3m from left, 20cm forward from whiteboard/paper wall
     toc; display('finished D* planning'); 
     cost_map_      = ds.costmap_get();
     distance_map_  = ds.distancemap_get();
     robot_location_in_world = camera_location_in_world+100*robot_pose(1:3,4)';     
     robot_location_in_world = uint16(robot_location_in_world/10);
%      ds.path([robot_location_in_world(1),robot_location_in_world(2)]);  % TODO - ds.goal(camera_location); ds.path(robot_location) 
     ds.path( [60, 20] ); % find the path to Matt's boat
     hold on;     plot(300/10,20/10,'gx')
     end;
     
     
    % Splines? - http://au.mathworks.com/help/curvefit/smoothing-splines.html --
    %
    % Reactive Path Following - bit like pure pursuit, but 
    % http://www.valvesoftware.com/publications/2009/ai_systems_of_l4d_mike_booth.pdf
    % slides 12-28 (illustrates the point-by-point look-ahead) (and p29 for reaction to obstacle - climb)
    % NOTE: slides 18 & 19; looks like the robot is moved away from the
    % corner ('Reactive') ; the right-hand close sensor/movement zone goes from pink (slide 18) to red (slide 20), so either the 
    % planned path is moved outward, or the robot does local avoidance as
    % it approaches the corner ~ force-field
    
    
    %% So - today - D* to plan and re-plan, 
    %       Pure Pursuit to follow the path ( then later Reactive Path Following to follow the path )
    %       VO/stereo/sonar on the Nao detect obstacles and update the map 
    
    
    % Need a map - convert the 3d points already known into a map, or grab it from the floorplan, or combine 
    % 
    
    % Just found the ROS Global Planner -
    % http://wiki.ros.org/global_planner 
    %   standard navigation stack needs sensor_msgs/PointCloud (or sensor_msgs/LaserScan)  
    %   - use stereo from the Nao cameras e.g. http://answers.ros.org/question/39068/running-turtlebot-with-viso2_ros-linear-odometry-now-working/
    %       - block match based? - http://wiki.ros.org/stereo_image_proc/Tutorials/ChoosingGoodStereoParameters
    %   -  http://wiki.ros.org/navigation/Tutorials/RobotSetup 
    %       For Nao - see http://wiki.ros.org/footstep_planner#footstep_navigation_node
    %           Example of walking _without_ planning - http://docs.ros.org/indigo/api/nao_apps/html/nao__walker_8py_source.html    
    %
    % And an alternative - https://github.com/lltb/footstep-planner
    %   including an example of converting an image to an occupancy grid 
    %
    %
    % http://wiki.ros.org/rtabmap_ros - RGBD, but stereo might be enough
    %
    % http://wiki.ros.org/stereo_image_proc
    %   stereo_image_proc will also compute disparity images from incoming stereo pairs using OpenCV's block matching algorithm. 