


function subimage_vertices = subimage_vertices_with_overlap_and_border(im1g, num_divs_, overlap_px_, border_px_)

    image_size = size(im1g);
    border_px = border_px_;
    if border_px < 0, border_px = 0; end;
    image_size = image_size - [border_px*2 , border_px*2];
    vertical_divs=num_divs_;
    if vertical_divs < 1, vertical_divs=1; end;
    horizontal_divs=num_divs_;
    if horizontal_divs < 1, horizontal_divs=1; end;
    subimage_vertices = uint16(vertical_divs*horizontal_divs, 4);
    overlap_px = overlap_px_;
    if overlap_px < 0, overlap_px = 0; end;
    sub_image_height=ceil(image_size(1)/horizontal_divs);
    sub_image_width=ceil(image_size(2)/horizontal_divs);
    
    subimage_vertices_index = 0;

    for vertical_div = 0:vertical_divs-1
        if vertical_div == 0
            sub_image_top = border_px; %vertical_div*sub_image_height;
        else
            sub_image_top = vertical_div*sub_image_height - overlap_px;
        end
        if vertical_div < vertical_divs-1
            sub_image_bottom = (vertical_div+1)*sub_image_height + overlap_px;
        else
            sub_image_bottom = image_size(1);
        end
        for horizontal_div = 0:horizontal_divs-1
            if horizontal_div > 0 
                sub_image_left = horizontal_div*sub_image_width - overlap_px;
            else
                sub_image_left = border_px; %horizontal_div*sub_image_width;
            end
            if horizontal_div < horizontal_divs-1
                sub_image_right = (horizontal_div+1)*sub_image_width + overlap_px;
            else
                sub_image_right = image_size(2);
            end
            subimage_vertices_index = subimage_vertices_index + 1;
            subimage_vertices(subimage_vertices_index)=[sub_image_top sub_image_bottom sub_image_left sub_image_right];
        end        
    end   
    
    % for next level of overlap
    vertical_offset_px   = ceil(sub_image_height/2);
    horizontal_offset_px = ceil(sub_image_width/2);
    
end