%{
Calibrated, registered fixed cameras.
Robot with calibrated camera.
Robot picks features in field of view, pushes to network.
Cameras match features, push back pose+rays+error_estimates /or/
pose+feature_positions+error_estimates.
    May include multiple matches if the feature is not discriminative enough.
Robot constructs and traverses graph. 
%}


% detect_and_draw_sift_features(image_, figure_handle_,edge_threshold_::10,peak_threshold_)

% base_dir = 'H:\Documents\ownCloud'
base_dir = 'D:\_will\ownCloud'

vl_feat_edge_threshold_default=10;
vl_feat_peak_threshold_default=0;

im = imread(fullfile('base_dir','\project_AA1__1_1','robot_3_pose_1 (1).jpg'));
image_gray = single(rgb2gray(image_));
[features_,descriptors_] = vl_sift(image_gray,'EdgeThresh',edge_threshold_, 'PeakThresh', peak_threshold_);







