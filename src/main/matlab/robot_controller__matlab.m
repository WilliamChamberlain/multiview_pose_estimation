% robot controller
%
% Sections indicate which will become the responsibilities of robot, VOS, and smart camera.
% 
% 


% Coordinate frame is the system coordinate frame 
%   facing the back wall of the lab from the paper submissions board; 
%     0,0,0 is the bottom left corner of the back wall, 
%     +ve x is right, 
%     +ve y is forward (through the wall toward Ag
%     +ve z is upward
%     see the coordinate frame in cam_1_pose_1_calc.m 



%% In the robot - 
[robot_image_ , robot_and_image_info_] = robot_get_image();
goal = rpy2tr(0,0,0);  % drive the robot camera to the floor at the origin 


%% In VOS - 
[camera_image_ , camera_and_image_info_] = camera_get_image();



% Matlab version - uses OpenCV 
[num_octaves, num_scales] = calculate_num_octaves_and_scales();
clipping_border = 50;
clipped_roi = [clipping_border clipping_border size(I,2)-clipping_border size(I,1)-clipping_border];
robot_points = detectSURFFeatures(imono(robot_image_), 'NumOctaves', num_octaves, 'NumScaleLevels', num_scales, 'ROI', clipped_roi);

clipping_border = 50;
clipped_roi = [clipping_border clipping_border size(I,2)-clipping_border size(I,1)-clipping_border];
camera_points = detectSURFFeatures(imono(camera_image_), 'NumOctaves', num_octaves, 'NumScaleLevels', num_scales, 'ROI', clipped_roi)

matchFeatures(robot_points,camera_points)


% Peter's version
% [Fund_model, residual, ransac_stats] = matches.ransac(@fmatrix, ransac_threshold_, 'verbose','maxTrials',1e5,'maxDataTrials',1e4);

