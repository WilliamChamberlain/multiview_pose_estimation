% April Tags - to get homography automatically -

% MIT implementation by Dr. Michael Kaess - http://people.csail.mit.edu/kaess/apriltags/
% /mnt/nixbig/build_workspaces/apriltags/tags      # - printable PDFs  
% /mnt/nixbig/build_workspaces/apriltags/src/Homography33.cc

% https://botaohu.wordpress.com/2015/03/10/use-apriltag-in-matlab/ 

% setup terminals - /mnt/nixbig/ownCloud/project_AA1__1_1/__README.txt

% turn the head tracking off - http://doc.aldebaran.com/2-1/nao/nao_freeze.html#nao-freeze
%   doc.aldebaran.com/2-1/nao/nao_life.html

% publish images / video from Nao through python  -  do so that can get
% camera info as well as the image - naoqi bringup only publishes
% low-quality images
% - http://answers.ros.org/question/199294/publish-image-msg/
% - https://github.com/rapyuta/rce/blob/master/rce-core/rce/util/converters/image.py
% - http://answers.ros.org/question/212347/how-to-publish-images-quickly/

% traversible space model: robot looks at feet / observes the world as it traverses - e.g. Patrick Ross, Steve Martin  

start_ros();

cctv_1_im_url       = '/cctv_cam_1/image_raw';   cctv_1_info_url = '/cctv_cam_1/camera_info';
cctv_1_im_ros_sub = rossubscriber(cctv_1_im_url,'sensor_msgs/Image');
cctv_1_cam_info_ros_sub = rossubscriber(cctv_1_info_url,'sensor_msgs/CameraInfo');
    cctv_1_im_obj = receive(cctv_1_im_ros_sub);   cctv_1_im = cctv_1_im_obj.readImage();
    idisp(cctv_1_im)
%         cctv_1_im = imresize(cctv_1_im, 360/size(cctv_1_im,1));
        cctv_1_im_info = receive(cctv_1_cam_info_ros_sub);      % uncalibrated --> useless
        cctv_1_im_cam_params=reshape(cctv_1_im_info.K,3,3)';    % uncalibrated --> useless
        [cctv_1_homography_Matrix_, cctv_1_pin, cctv_1_pout] = cctv_1_homography_calculate_and_apply();
        [cctv_1_rectifiedImage,cctv_1_offset] = homwarp(cctv_1_homography_Matrix_, cctv_1_im, 'full'  );
        fig_handle = figure('Name','rectified fixed camera'); idisp(cctv_1_rectifiedImage, 'figure', fig_handle)
        cctv_1_pin = vertcat(cctv_1_pin, ones(1,size(cctv_1_pin,2)));
        cctv_1_pout = vertcat(cctv_1_pout, zeros(1,size(cctv_1_pout,2)));        
        cctv_1_pout = vertcat(cctv_1_pout, ones(1,size(cctv_1_pout,2)));        
        load('/mnt/nixbig/data/camera_calibration/MATLAB_C920_Logitech/cameraParams_C920_Logitech.mat')
        % save('/mnt/nixbig/data/camera_calibration/MATLAB_C920_Logitech/cameraParams_C920.mat', 'cameraParams_C920_Logitech')
        [cctv_1_R,cctv_1_T,cctv_1_Xc,cctv_1_best_solution]=efficient_pnp(cctv_1_pout', cctv_1_pin', cameraParams_C920_Logitech.IntrinsicMatrix);  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
        %[cctv_1_R,cctv_1_T,cctv_1_Xc,cctv_1_best_solution]=efficient_pnp(cctv_1_pout, cctv_1_pin, cameraParams_Kogan_Agora_Lite.IntrinsicMatrix);  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
        %[cctv_1_R,cctv_1_T,cctv_1_Xc,cctv_1_best_solution]=efficient_pnp(cctv_1_pout, cctv_1_pin, cameraParams.IntrinsicMatrix);  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
        %cameraParams_C920_Logitech = cameraParams;
        %cameraParams_Kogan_Agora_Lite
        cctv_1_T
        cctv_1_T_=cctv_1_T./(cctv_1_T(3)/110)
%         
% cctv_2_im_url       = 'http://192.168.1.148:8080/shot.jpg';   
%      cctv_2_im = imread(cctv_2_im_url); cctv_2_info = imfinfo(cctv_2_im_url); cctv_2_data = webread(cctv_2_im_url); 
%      figure('Name','CCTV_2 plain'); imshow(cctv_2_im)
%         cctv_2_homography_Matrix_ = cctv_2_homography_calculate_and_apply();
%         [cctv_2_rectifiedImage,cctv_2_offset] = homwarp(cctv_2_homography_Matrix_ , cctv_2_im, 'full'  );
%         fig_handle = figure('Name','rectified fixed camera'); idisp(cctv_2_rectifiedImage, 'figure', fig_handle)
%      
cctv_3_im_url       = 'http://192.168.1.173:8080/shot.jpg'; 
     cctv_3_im = imread(cctv_3_im_url); cctv_3_info = imfinfo(cctv_3_im_url); cctv_3_data = webread(cctv_3_im_url);   
     figure('Name','CCTV_3 plain'); imshow(cctv_3_im)     
        cctv_3_homography_Matrix_ = cctv_3_homography_calculate_and_apply();
        [cctv_3_rectifiedImage,cctv_3_offset] = homwarp(cctv_3_homography_Matrix_ , cctv_3_im, 'full'  );
        fig_handle = figure('Name','rectified fixed camera'); idisp(cctv_3_rectifiedImage, 'figure', fig_handle)
     
        

robot_im_url       = 'http://192.168.1.148:8080/shot.jpg';   
     robot_im = imread(robot_im_url); robot_info = imfinfo(robot_im_url); robot_data = webread(robot_im_url); 
     figure('Name','robot plain'); imshow(robot_im)
        robot_homography_Matrix_ = robot_homography_calculate_and_apply();
        [robot_rectifiedImage,robot_offset] = homwarp(robot_homography_Matrix_ , robot_im, 'full'  );
        fig_handle = figure('Name','rectified fixed camera'); idisp(robot_rectifiedImage, 'figure', fig_handle)
           

% cctv_4_im_url       = 'http://192.168.1.178:8080/shot.jpg';   
%      cctv_4_im = imread(cctv_4_im_url); cctv_4_info = imfinfo(cctv_4_im_url); cctv_4_data = webread(cctv_4_im_url);     
% robot_ipwebcam_url  = 'http://192.168.1.148:8080/shot.jpg';
    %   % can't get the resolution up ; use an IP Webcam  :      robot_im = receive(robot_image_ros_subscriber);  robot_im_camera_param_matrix=reshape(robot_im_info.K,3,3)'; robot_im_im = robot_im.readImage();    
%     robot_im = imread(robot_ipwebcam_url); robot_ipwebcam_info = imfinfo(robot_ipwebcam_url); robot_ipwebcam_data = webread(robot_ipwebcam_url); 
% imwrite(cctv_1_im, '/mnt/nixbig/ownCloud/project_AA1__1_1/camera_1_1(1).tiff','Compression','none','Resolution',100) 
% % imwrite(cctv_2_im, '/mnt/nixbig/ownCloud/project_AA1__1_1/camera_2_1(1).tiff','Compression','none','Resolution',100) 
% imwrite(cctv_3_im, '/mnt/nixbig/ownCloud/project_AA1__1_1/camera_3_1(1).tiff','Compression','none','Resolution',100) 
% imwrite(cctv_4_im, '/mnt/nixbig/ownCloud/project_AA1__1_1/camera_4_1(1).tiff','Compression','none','Resolution',100) 
% imwrite(robot_im, '/mnt/nixbig/ownCloud/project_AA1__1_1/robot_1_1(1).tiff','Compression','none','Resolution',100) 

    % would loop here

%         robot_im = imresize(robot_im, 360/size(robot_im,1));  
% %     cctv_2_im = resize(cctv_2_im,360/size(cctv_1_im,1));
% %     cctv_3_im = resize(cctv_3_im,360/size(cctv_1_im,1));
    idisp({robot_im, cctv_3_im}); %, cctv_2_im, cctv_3_im})
    
    filename = '/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_lab_2016_11_14/cctv_1_x_1.tif';  imwrite(cctv_1_im, filename,'Compression','none','Resolution',300)           
    
    % segment floorspace by HSV : sparse, but closing or counting proportion per unit area will do
    channel1Min = 0.545; channel1Max = 0.57; satMin = 0.20; satMax = 0.55; %  TODO - hardcoding - illumination invariance from floor appearance model as viewed by robot
    hsvRectifiedImage = rgb2hsv(cctv_1_rectifiedImage);
    freeFloorMask = (hsvRectifiedImage(:,:,1) >= channel1Min ) & (hsvRectifiedImage(:,:,1) <= channel1Max) & (hsvRectifiedImage(:,:,2) >= satMin ) & (hsvRectifiedImage(:,:,2) <= satMax);    
    maskedRGBImage = cctv_1_rectifiedImage;
    maskedRGBImage(repmat(freeFloorMask,[1 1 3])) = 0;  figure('Name','Floor free space in fixed camera view'); imshow(maskedRGBImage);
    

s_r1_base = isift(robot_im);           % robot's features
s_c1_base = isift(cctv_3_im);           % fixed camera's features
% % s_c2_base = isift(cctv_2_im);           % fixed camera's features
% % s_c3_base = isift(cctv_3_im);           % fixed camera's features
s1=s_r1_base;
s2=s_c1_base;
s1=s_r1_base(s_r1_base.scale>median(s_r1_base.scale)); % remove the fine-scale features: TO INVESTIGATE - control the octaves, or downsample?
s2=s_c1_base(s_c1_base.scale>median(s_r1_base.scale)); % remove the fine-scale features
% size(webcam_next_image_im)./size(next_image_im)

% [m1to2easy,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','euclidean','next_match_ratio',1.001,'median');
% m1to2hard = m1to2easy
% [m2to1easy,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','euclidean','next_match_ratio',1.001,'median');
[m1to2hard,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','hellinger','next_match_ratio',2,'median');
[m2to1hard,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','hellinger','next_match_ratio',2,'median');


f_handle = figure;
idisp({robot_im,cctv_3_im},'figure',f_handle)
hold on;
m1to2hard.plot('m')
plot(s1(corresp1to2(1,:)),'gx')
% temp = s2(corresp1to2(2,:));
% hold on; plot( temp.u + size(next_image_im,1)  , temp.v , 'gx')
f_handle = figure;
hold off; idisp({cctv_3_im,robot_im},'figure',f_handle)
hold on;  m2to1hard.plot('y')
plot(s2(corresp2to1(1,:)),'gx')
        % temp = s2(corresp2to1(2,:));
        % hold on; plot( temp.u + size(webcam_next_image_im,1)  , temp.v , 'gx')
f_handle = figure;
[H_m1to2,r_H_m1to2] = m1to2hard.ransac(@homography,1e-2,'maxTrials',50000, 'maxDataTrials',1000);
hold off; idisp({robot_im,cctv_3_im},'title','m1to2hard.ransac(@homography,1e-2','figure',f_handle)
hold on;  m1to2hard.plot('m')
plot(s1(corresp1to2(1,:)),'gx')
hold on;
m1to2hard.inlier.plot('g--')
good_H_m1to2 = H_m1to2;
good_m1to2hard = m1to2hard;

f_handle = figure;
[H_m2to1,r_H_m2to1] = m2to1hard.ransac(@homography,1e-2,'maxTrials',5000, 'maxDataTrials',500);
hold off; idisp({cctv_3_im,robot_im},'title','m2to1hard.ransac(@homography,1e-2','figure',f_handle)
hold on;  m2to1hard.plot('y')
plot(s2(corresp2to1(1,:)),'bx')
hold on;
m2to1hard.inlier.plot('b--')
good_H_m2to1 = H_m2to1;
good_m2to1hard = m2to1hard;

% GOT TO HERE 2016_11_22__21:50
% TODO: do the MATLAB pose estimation 

cameraParams_nexus_5 = cameraParams; %from cam_1_pose_1_calc.m

% C920 Logitech - ??  3.67mm focal length 
%   C920 6.00mm W4.80mm H3.60mm (16:9 format) - http://lukse.lt/uzrasai/2013-07-modifying-logitech-c920-to-for-cs-lenses/
%   ?? https://www.learnopencv.com/approximate-focal-length-for-webcams-and-cell-phone-cameras/
%   

% Agora Lite
cam1 = CentralCamera('image',robot_im,'sensor', [2.50e-3 , 3.33e-3],'focal',3.458*0.001);
S=cam1.invH(H_m1to2);
S.n
S.T
S(2).T
S(1).T
tr2rpy(S(2).T)

% Nexus 5  - http://www.gsmarena.com/lg_nexus_5-5705.php
cam1 = CentralCamera('image',cctv_3_im,'sensor', [1.4e-3 , 1.4e-3],'focal',2.87*0.001);
S=cam1.invH(H_m2to1);
S.n
S.T
S(2).T
S(1).T
tr2rpy(S(2).T)


cam1 = CentralCamera('image',robot_im,'sensor', [2.50e-3 , 3.33e-3],'distortion',[0.084930 -0.153198 0.011283 -0.000882 0.000000],'focal',3.67*0.001);
S=cam1.invH(H_m1to2);
S.n
S.T
S(1).T
tr2rpy(S(2).T)


m1to2 = m1to2hard;
[F_m1to2,r] = m1to2.ransac(@fmatrix,1e-2,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({robot_im,cctv_3_im},'title','inliers m1to2.ransac(@fmatrix', 'figure', inliers_F_m1to2_fig)
m1to2.inlier.plot();


% [F,r] = m1to2.ransac(@fmatrix,1e-3,'verbose','maxTrials',5000, 'maxDataTrials',1000);
[F_m1to2,r] = m1to2hard.ransac(@fmatrix,1e-4,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig2 = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({robot_im,cctv_3_im},'title','inliers m1to2.ransac(@fmatrix', 'figure', inliers_F_m1to2_fig2)
m1to2.inlier.plot();

%% GOOD!
[F_m2to1,r_m2to1] = m2to1hard.ransac(@fmatrix,1e-4,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m2to1_fig = figure('Name','inliers m2to1hard.ransac(@fmatrix');
idisp({cctv_3_im,robot_im},'title','inliers m2to1hard.ransac(@fmatrix', 'figure', inliers_F_m2to1_fig)
m2to1hard.inlier.plot();

[F_m1to2,r] = m1to2hard.ransac(@fmatrix,1e-3,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig3 = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({robot_im,cctv_3_im},'title','inliers m1to2.ransac(@fmatrix,1e-3', 'figure', inliers_F_m1to2_fig3)
m1to2.inlier.plot();

%% GOOD!
[F_m2to1,r_m2to1] = m2to1hard.ransac(@fmatrix,1e-3,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m2to1_fig = figure('Name','inliers m2to1hard.ransac(@fmatrix');
idisp({cctv_3_im,robot_im},'title','inliers m2to1hard.ransac(@fmatrix,1e-3','figure',inliers_F_m2to1_fig)
m2to1hard.inlier.plot();

%% Got to here 2016_11_22_22:27
cctv_3_pos = [160;110;99];
rob_pos = [134;67;41];


known_separation_x =  160-134;
known_separation_y =  110-67;
known_separation_z =  99-41; 
E = cam1.E(F_m2to1);
                sol = cam1.invE(E, [0,0,10]');
                [R,t] = tr2rt(sol); t
%                 t = known_separation_x * t/t(1);                           %  !  prior - 1m between photos
%                 t = known_separation_y * t/t(2);
                t = known_separation_z * t/t(3); t
                T = rt2tr(R, t);
                [R,t] = tr2rt(T);
                rpy_ = tr2rpy(R, 'deg');
                translation_= t;      translation_
                
% rosshutdown  figure
% freeFloorMask_ = freeFloorMask;
% freeFloorMask = freeFloorMask(1:size(freeFloorMask,1)-100, 1:size(freeFloorMask,2));
figure; imshow(freeFloorMask);

grid_to_px = 10;
floorplan = ones(size(freeFloorMask));
for i_ = 1:size(freeFloorMask,1)/grid_to_px         %   Y 
    for j_ = 1:size(freeFloorMask,2)/grid_to_px     %   X 
        hold on;
        plot( ...
            [ (j_*grid_to_px)+1, ( (j_-1) * grid_to_px)+1, ( (j_-1) * grid_to_px), ( (j_) * grid_to_px)+1, ( (j_) * grid_to_px)+1 ],  ...
            [ ( (i_-1)*grid_to_px)+1, ( (i_-1)*grid_to_px)+1, (i_*grid_to_px), (i_*grid_to_px), ( (i_-1)*grid_to_px)+1 ] ...
        )
        if sum( sum( freeFloorMask( ( (i_-1)*grid_to_px)+1:( (i_)*grid_to_px) , ( (j_-1)*grid_to_px)+1:( (j_)*grid_to_px) ) ) ) > 50
            plot( ( (j_-1) * grid_to_px) + 1 + (grid_to_px/2), ((i_-1) * grid_to_px) + 1 + (grid_to_px/2) , 'go');
            plot( ( (j_-1) * grid_to_px) + 1 + (grid_to_px/2), ((i_-1) * grid_to_px) + 1 + (grid_to_px/2) , 'gx');
            floorplan(  ((j_-1) * grid_to_px) + 1  : j_ * grid_to_px ,  ((i_-1) * grid_to_px) + 1 : i_ * grid_to_px )  = 1;
        else
            plot( ( (j_-1) * grid_to_px) + 1 + (grid_to_px/2), ((i_-1) * grid_to_px) + 1 + (grid_to_px/2), 'rx');
            floorplan(  ((j_-1) * grid_to_px) + 1  : j_ * grid_to_px ,  ((i_-1) * grid_to_px) + 1 : i_ * grid_to_px )  = 0;
        end        
    end
end

figure; idisp(floorplan); hold on; plot(cctv_1_pos(2),cctv_1_pos(1), 'rx');
planner_floorplan = floorplan;
planner_floorplan(floorplan>0) = Inf; 
figure; idisp(planner_floorplan);
dstar_planner = Dstar(planner_floorplan);
cost_map = dstar_planner.costmap_get();
figure; idisp(cost_map);
goal=[233,87];
dstar_planner.plan(goal);
start=[135,330];
figure;
dstar_planner.path(start);


