
try  rostopic list;   display('The MATLAB ROS global node is already running.');
catch ME,  display('The MATLAB ROS global node is not running yet: running rosinit.'); 
    rosinit; rostopic list;
end;