function homography_Matrix_ = homography_calculate_and_apply()   

%     downloads_dir = 'C:\downloads'; % getenv('DOWNLOADS_DIR');
%     
%     if isempty(strfind(path,'machine_vision_toolbox-3.4'))
%         addpath( genpath( [  '\' 'machine_vision_toolbox-3.4' ] ));
%     end        
%     if isempty(strfind(path,'matlab_Oxford_robotics_Multiple_View_Geometry_allfns'))
%         addpath( genpath( [ downloads_dir '\' 'matlab_Oxford_robotics_Multiple_View_Geometry_allfns' ] ));
%     end    
    
% Set Machine Vision Toolbox in the path
% Set Oxford Robotics matlab functions in the path
% C:\downloads\matlab_Oxford_robotics_Multiple_View_Geometry_allfns

%     Uses Oxford Robotics  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/vgg_multiview/vgg_H_from_x_lin.m


% PIN are pixel coordinates : for camera 1   
% from  undistorted_image=undistortImage(firstFrame,cameraParams,'OutputView','full');
%
    PIN = [
            170,  76;
            643,  89;
            246, 358;
            586, 370;
            966, 382;
        ]';
% plot onto the webcam image   
%     figure; hold on; plot(PIN(1,:),PIN(2,:),'ro');
% POUT are world coordinates in centimetres 
room_depth_x              = 643.5;
room_width_y              = 780.0;
x_offset                  = 0;
y_offset                  = 400;
    POUT = [ 
         55, y_offset-0;
        155, y_offset-0;
%          55, y_offset-120;
        105, y_offset-120;
        155, y_offset-120;
        205, y_offset-120;
%         350, y_offset-150;
%         440, y_offset-150;
%          55, y_offset-220; 
%         105, y_offset-220;
%         155, y_offset-220;
%         205, y_offset-220;
%         350, y_offset-220;
%         400, y_offset-220;
%         440, y_offset-220;         
        ]';
%     figure; hold on; plot(POUT(1,:),POUT(2,:),'bo'); plot(0,0,'gx')
    
homography_Matrix_ = homography(PIN, POUT);  % http://www.petercorke.com/MVTB/vision.pdf

end

