% Robotics, Vision and Control p400-402

% Estimate the variance of the relative camera pose 
% - from the essential matrix estimation - 
% with the residual.
for ratio = 1.01:0.6:1.61
for reduction = 4:-1:1
for pic1_num = 10:4:40 
for pic2_num = pic1_num+22:2:50 
[im1 tags]=iread(sprintf('/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_2016_10_18/S11_lab_%d00.jpg',pic1_num),'reduce',reduction,'double','mono');
focal_length = double(tags.DigitalCamera.FocalLength) * 1e-3;
% im2=iread('/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_2016_10_18/S11_lab_1600.jpg','double','mono');  
im2=iread(sprintf('/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_2016_10_18/S11_lab_%d00.jpg',pic2_num),'reduce',reduction,'double','mono');  
baseline_metres = abs((pic2_num - pic1_num)/10);
filename = ...
    sprintf( ...
        strcat('/mnt/nixbig/ownCloud/project_AA1__1_1/peter_RVC_example_results/' , ...
        'Peters_RVC_example_residual_and_translation_estimates_Will_pics_%d00_%d00__red%d__rat%d.mat'),pic1_num,pic2_num,reduction,ratio);



% s1 = isurf(im1);  % 691 corners found 
% s2 = isurf(im2);  % 699 corners found (0.2%)
s1 = isift(im1,'nfeat',1000);
s2 = isift(im2,'nfeat',1000);  % 3086 corners found (0.8%)
% m = s1.match(s2);
% m.show()  
% Ubuntu 14.04 64-bit:
% ans =
% 132 corresponding points
% Book lists "323 corresponding points"
% % m = s1.match(s2,'median');
% m = matchFeatures(s1,s2,'distance_metric','cosine','next_match_ratio',1.01,'median');
m = matchFeatures(s1,s2,'distance_metric','hellinger','next_match_ratio',1.01,'median');
% % m.show;
% ans =
% 165 corresponding points

% looks like a good correspondence set!
% % figure;idisp({im1,im2}); m.plot();

% % % This is pretty good!!
% [H,r] = m.ransac(@homography,2,'maxTrials',10000,'maxDataTrials',1000);
[H,r] = m.ransac(@homography,2);
% % m.show;
% ans =
% 165 corresponding points
% 52 inliers (31.5%)
% 113 outliers (68.5%)

% points on the wall, the plants in the pots, the bush growing up against
% the wall - only approximately planar/homography
% % idisp({im1,im2})
% % m.inlier.plot() 

% % m2=m.outlier;

% % % This is pretty good!!
% % [H2,r2] = m2.ransac(@homography,2,'maxTrials',20000,'maxDataTrials',1000);


% % m2.show
% ans =
% 113 corresponding points
% 24 inliers (21.2%)
% 89 outliers (78.8%)


% points on the bush growing up against the wall, the exhaust vents, 
% the wall, the plants in the pots,  - only approximately planar/homography

% % idisp({im1,im2})
% % m2.inlier.plot() 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % m = matchFeatures(s1,s2,'distance_metric','cosine','next_match_ratio',1.01,'median');
%165 NOT 323 

cam = CentralCamera('image',im1,'focal',focal_length,'pixel',1.4e-6,'resolution',[3264,2448]);
[F,r] = m.ransac(@fmatrix,1e-4,'verbose');
% % cam.plot_epiline(F', m.inlier.subset(30).p2, 'r');
E_   =zeros(3,3,100,'double');
fund_=zeros(3,3,100,'double');
r_ = zeros(1,100,'double');
rpy_ = zeros(1,3,100,'double');
sol_=zeros(4,4,100,'double');
T_=zeros(4,4,100,'double');

    
for i_ = 1:100
    try
%         [F,r] = m.ransac(@fmatrix,1e-4,'maxTrials',10000,'maxDataTrials',1000);
        [F,r] = m.ransac(@fmatrix,1e-4);
        fund_(:,:,i_) = F;
        r_(i_) = r;
        E = cam.E(F);
        E_(:,:,i_) = E;
        sol = cam.invE(E,[0,0,10]');
        sol_(:,:,i_) = sol;
        [R,t] = tr2rt(sol);
        rpy=tr2rpy(R,'deg');
        rpy_(:,:,i_) = rpy;
        t = baseline_metres * t/t(1);
        T = rt2tr(R,t);
        T_(:,:,i_) = T;
    catch 
        display(sprintf('Error at iteration %d; continuing.',i_));
    end
end;
save(filename);
filename = ...
    sprintf( ...
        strcat('/mnt/nixbig/ownCloud/project_AA1__1_1/peter_RVC_example_results/' , ...
        'Peters_RVC_example_residual_and_translation_estimates_Will_pics_%d00_%d00__red%d__rat%d__residuals.png'),pic1_num,pic2_num,reduction,ratio);
figure; plot(r_); legend('Residual per iteration');
print(filename,'-dpng');


filename = ...
    sprintf( ...
        strcat('/mnt/nixbig/ownCloud/project_AA1__1_1/peter_RVC_example_results/' , ...
        'Peters_RVC_example_residual_and_translation_estimates_Will_pics_%d00_%d00__red%d__rat%d__translations.png'),pic1_num,pic2_num,reduction,ratio);
figure; hold on; plot(squeeze(T_(1,4,:)));plot(squeeze(T_(2,4,:)));plot(squeeze(T_(3,4,:))); legend('X','Y','Z');
print(filename,'-dpng');

close all;
% clear variables;
end;
end;
end;
end;
