
function corrected_coordinates_ = coords_reflect_and_offset(coordinates_, nominal_zero_coordinate_, coordinate_dimension_)
    corrected_coordinates_      = coordinates_;
    num_coordinates             = size(coordinates_,1);
    corrected_coordinates_(:,coordinate_dimension_) = ( ones(num_coordinates,1).*nominal_zero_coordinate_(coordinate_dimension_)) - coordinates_(:,coordinate_dimension_);
end