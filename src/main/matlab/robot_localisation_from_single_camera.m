% [Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose')

% Estimate via essential or fundamental matrices from SURF matches of two
% views of S11 labs
% Matching - Robotics, Vision and Control pp385-6
%   parameters are
%       distance measure
%       threshold
%           Robotics, Vision and Control pp385-6
%               threshold options: 'top', 'thresh', 'median'
%               descriptor matching threshold: zero-mean normalized cross-correlation ZNCC gives correspondence strength between descriptors of points - Robotics, Vision and Control pp383-384
% Use for estimation
%   five point from Vincent
%   eight point from Hartley
%       Peter's
% Use RANSAC: loss function is reprojection error
%   parameters are 
%         loss function
%         threshold
%         iterations
% 
%   Integration between fmatrix() and ransac()
%       fmatrix.ransac_driver is the function that operates within and e.g. sets sample size=8
%       
% fmatrix()
%   conditioning of data: normalise to zero mean and standard deviation = sqrt(2): uses
%       vgg_conditioner_from_pts, vgg_condition_2d
% 
% inliers:  [bestInliers, bestF] = funddist(F, x, t)  does give inliers, but ransac(@fmatrix,...) does not
% inliers are updated in the matches object
% 
% usage: [Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose','maxTrials',1e4)
% 
%
% NOT process for VOS - that is based on Lie algebra, with known baseline to get scale
% 
% See Robotics, Vision and Control pp391-394 14.2.3 Estimating the Fundamental Matrix - SURF etc - 
%     F = m.ransac(@fmatrix, 1e-4, 'verbose')
%   Fundamental Matrix defn and equations - pp388-9
%   Essential Matrix defn and equations - pp390-1
%       From Fundamental Matrix - cam1.invE(E)
%       check by reprojection of point in front of camera - Q = [0 0 10]'; cam1.move(sol(:,:,1)).project(Q)
%           implemented as sol = cam1.invE(E, Q)

% isurf 
%       The SurfPointFeature object has many properties including:
%        u            horizontal coordinate
%        v            vertical coordinate
%        strength     feature strength
%        descriptor   feature descriptor (64x1 or 128x1)
%        sigma        feature scale
%        theta        feature orientation [rad]
% 
%       Options::
%       'nfeat',N      set the number of features to return (default Inf)
%       'thresh',T     set Hessian threshold.  Increasing the threshold reduces
%                      the number of features computed and reduces computation time.
%       'octaves',N    number of octaves to process (default 5)
%       'extended'     return 128-element descriptor (default 64)
%       'upright'      don't compute rotation invariance
%       'suppress',R   set the suppression radius (default 0).  Features are not
%                      returned if they are within R [pixels] of an earlier (stronger)
%                      feature.
% 
% 
% 
% 
% See https://au.mathworks.com/help/vision/examples/uncalibrated-stereo-image-rectification.html
%       RANSAC	RANdom SAmple Consensus. Select this method if you would like to set the distance threshold for the inliers.
%       MSAC	M-estimator SAmple Consensus. Select the M-estimator SAmple Consensus method if you would like to set the distance threshold for the inliers. Generally, the MSAC method converges more quickly than the RANSAC method.
%         'DistanceType' � Algebraic or Sampson distance type
%             'Sampson' (default) | 'Algebraic'
%         'DistanceThreshold' � Distance threshold for finding outliers
%             0.01 (default)
%         'ReportRuntimeError' � Report runtime error
%             true (default) | false
%         status � Status code
%             0 no error | 1 not enough points | 2 not enough inliers found
%         uses the 8-point algorithm:
%           http://www.cs.unc.edu/~marc/tutorial/node54.html
%           https://en.wikipedia.org/wiki/Eight-point_algorithm
%           http://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/f07/proj_final/www/amichals/fundamental.pdf
% 
% 
% !!! https://au.mathworks.com/help/vision/ref/relativecamerapose.html !!!
% https://au.mathworks.com/help/vision/examples/structure-from-motion-from-two-views.html
% https://au.mathworks.com/help/vision/examples/structure-from-motion-from-multiple-views.html
% 
% 
% http://openmvg.readthedocs.io/en/latest/openMVG/multiview/multiview/
% http://openmvg.readthedocs.io/en/latest/openMVG/robust_estimation/robust_estimation/
% http://openmvg.readthedocs.io/en/latest/openMVG/matching/matching/
% http://openmvg.readthedocs.io/en/latest/openMVG/tracks/tracks/
% http://openmvg.readthedocs.io/en/latest/openMVG/geometry/geometry/
% http://www.icg.tugraz.at/publications/inproceedings.2010-05-25.6505863218
% https://www.researchgate.net/publication/274094180_Factorization_of_View-Object_Manifolds_for_Joint_Object_Recognition_and_Pose_Estimation
% 
% 
% 
% https://au.mathworks.com/help/vision/ref/relativecamerapose.html
% 
% http://iitlab.bit.edu.cn/mcislab/~yangjiaolong/yang_et_al_eccv14_optimal_ematrix.pdf
% Optimal Essential Matrix Estimation via Inlier-Set Maximization
% 
% https://en.wikipedia.org/wiki/Random_sample_consensus
% 
% http://www.peterkovesi.com/matlabfns/index.html#projective


f_handle_1_ = figure('Name','im_1 im_2 num_div = 1')
f_handle_2_ = figure('Name','im_1 im_2 num_div = 2')
f_handle_3_ = figure('Name','im_1 im_2 num_div = 3')


im_ = imread('D:\_will\owncloud\project_AA1__1_1\robot_3_pose_1 (4).jpg');
im_2 = imread('D:\_will\owncloud\project_AA1__1_1\robot_2_pose_1 (1).jpg');

[im_1,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\robot_3_pose_1 (4).jpg');
[im_2,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\robot_2_pose_1 (1).jpg');

% opt.thresh = 0.0008;
tic;
[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages_and_border(f_handle_1_, im_1, im_2, 1, 5000, 10, 'upright', true);
toc; tic;
[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages_and_border(f_handle_2_, im_1, im_2, 1, 5000, 10, 'thresh', 0.004, 'upright', true);
toc; tic;
[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages_and_border(f_handle_3_, im_1, im_2, 1, 5000, 10, 'thresh', 0.002, 'upright', true);
toc;

[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages_and_border(f_handle_2_, im_1, im_2, 2, 5000, 10);

[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_subimages_and_border(f_handle_3_, im_1, im_2, 3, 5000, 10);

% suppress seems to block
% [SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_border(f_handle_3_, im_1, im_2, 3, 5e4, 10);

[SurfPointFeature_1 , SurfPointFeature_2 , matches , correspondences ] = surf_detect_match_display_with_border(f_handle_3_, im_1, im_2, -1, -1, 30);


[im_1,tags_1] = iread('D:\_will\owncloud\project_AA1__1_1\camera_4_pose_1 (2).jpg');
[im_2,tags_2] = iread('D:\_will\owncloud\project_AA1__1_1\camera_5_pose_1 (1).jpg');

[im_1,tags_1] = iread('D:\downloads\machine_vision_toolbox-3.4\images\rvctools\vision\images\garden-l.jpg');
[im_2,tags_2] = iread('D:\downloads\machine_vision_toolbox-3.4\images\rvctools\vision\images\garden-r.jpg');

s1 = isurf(im_1);
s2 = isurf(im_2);
m = s1.match(s2);

[Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose','maxTrials',1e5,'maxDataTrials',1e4)  % 100,000 trials, 1000 trials to find a non-degenerate set of inliers

[Fund_model, residual] = matches.ransac(@fmatrix, 1e-4, 'verbose');



% 
% https://en.wikipedia.org/wiki/Nexus_5
% https://en.wikipedia.org/wiki/Exmor
% Robotics, Vision and Control p282  , p403
camera__1 = CentralCamera( ...
    'name','camera__1',  ...    
    'image', im_1, ...
    'focal', mean(tags_1.DigitalCamera.FocalLength * 1e-3), ...    
    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
% camera1 = CentralCamera('image',im_);

% for Garden pictures
% camera__1 = CentralCamera('image', im_1, 'focal', mean(tags_1.DigitalCamera.FocalLength * 1e-3), ...
% 'sensor', [7.18e-3,5.32e-3]);
figure('Name','Camera 1 epilines of camera 2 inliers');
camera__1.plot_epiline(Fund_model',matches.inlier.p2,'g');
camera__1.plot_epiline(Fund_model',matches.inlier.subset(30).p2,'g');


camera__2 = CentralCamera( ...
    'name','camera__2',  ...    
    'image', im_2, ...
    'focal', mean(tags_2.DigitalCamera.FocalLength * 1e-3), ...    
    'sensor', [6.18e-3 , 5.85e-3]  );   %     'pixel',1.4e-6,  ...  %     'centre', cameraParams.PrincipalPoint,  ...  % 'resolution',[size(im_,2),size(im_,1)],  ...
% camera2 = CentralCamera('image',im_2);
 
% for Garden pictures
% camera__2 = CentralCamera('image', im_2, 'focal', mean(tags_2.DigitalCamera.FocalLength * 1e-3), ...
% 'sensor', [7.18e-3,5.32e-3]);
figure('Name','Camera 2 epilines of camera 1 inliers_');
camera__2.plot_epiline(Fund_model,matches.inlier.p1,'g');



% pp403-4
E = camera__1.E(Fund_model);
sol = camera__1.invE(E, [0,0,10]');
[R,t] = tr2rt(sol)
tr2rpy(R, 'deg')



E_ = camera__1.E(Fund_model');
sol_ = camera__1.invE(E_, [0,0,10]');
[R_,t_] = tr2rt(sol_)
tr2rpy(R_, 'deg')






E2 = camera__2.E(Fund_model);
sol2 = camera__1.invE(E2, [0,0,10]');
[R2,t2] = tr2rt(sol2)
tr2rpy(R2, 'deg')



E2_ = camera__2.E(Fund_model');
sol2_ = camera__1.invE(E2_, [0,0,10]');
[R2_,t2_] = tr2rt(sol2_)
tr2rpy(R2_, 'deg')


[~, ind] = sort([SurfPointFeature_1.]);
chairs_sorted = chairs(ind);



