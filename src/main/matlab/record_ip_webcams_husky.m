%{
  z  hostname                    IP              MAC
 51 android-b68ae593574701f9 192.168.1.136
 89 android-21da83a7e65e9940 192.168.1.131
141 android-8a1640242815d6e8 192.168.1.227


Initial position: 
facing bookcase=90 degrees anticlockwise from y axis (y axis from robot lab to cantina), 
y0=camera offset from wall = 44-41cm left of frame, 
x=camera offset from bookcase=301cm.
theta = 90

2nd 
y     = whatever
x     = 285 (not 301)
theta = 60

3rd 
y     = whatever
x     = 278 (not 301)
theta = 30

4th
y     = whatever
x     = 285
theta = 120



Apriltags centres on bookcase: 
    x :  0
    y : 13cm, 103cm, 193cm,   13cm, 103cm, 193cm
    z : 54cm,  54cm,   54cm, 144cm, 144cm, 144cm
on floor: 
    x : 38cm
    y : 124+180cm
    z : 18cm
on circusbot:
    x : 210 + 38
    y : 687 - 37
    z : 29
on door frame
    x : 12
    y : 270 + 88 = 358
    z : 188
%}

function [cctv_1_fhandle,cctv_2_fhandle,cctv_3_fhandle,cctv_4_fhandle] = record_ip_webcams_husky(x_, y_, theta_,cctv_1_fhandle,cctv_2_fhandle,cctv_3_fhandle,cctv_4_fhandle)
    for i_=1:1000
        date_time_str = datestr(now(),'yyyymmddHHMMSSFFF');
        tic
        if ~exist('cctv_1_fhandle','var'), cctv_1_fhandle = figure('Name','cctv_1 plain'); end;
        file_dir = '/mnt/nixbig/ownCloud/project_AA1__1_1/results/2016_12_01_1015_S11_lab/';
        file_suffix = 'tiff';
        cctv_1_im_url       = 'http://192.168.1.136:8080/shot.jpg'; 
        cctv_1_im = imread(cctv_1_im_url); 
        cctv_1_info = imfinfo(cctv_1_im_url); 
        cctv_1_data = webread(cctv_1_im_url);   
        figure(cctv_1_fhandle); imshow(cctv_1_im);
        cam_name = 'cctv_1'
        z = 51
        file_name = sprintf('%s%s_%s_%d_%d_%d_%d.%s',file_dir,date_time_str,cam_name,x_,y_,z,theta_,file_suffix)
        imwrite(cctv_1_im, file_name,'Compression','none','Resolution',100)     

        if ~exist('cctv_2_fhandle','var'), cctv_2_fhandle = figure('Name','cctv_2 plain'); end;
        cctv_2_im_url       = 'http://192.168.1.131:8080/shot.jpg'; 
        cctv_2_im = imread(cctv_2_im_url); 
        cctv_2_info = imfinfo(cctv_2_im_url); 
        cctv_2_data = webread(cctv_2_im_url);   
        figure(cctv_2_fhandle); imshow(cctv_2_im);
        cam_name = 'cctv_2'
        z = 141
        file_name = sprintf('%s%s_%s_%d_%d_%d_%d.%s',file_dir,date_time_str,cam_name,x_,y_,z,theta_,file_suffix)
        imwrite(cctv_2_im, file_name,'Compression','none','Resolution',100)     

         if ~exist('cctv_3_fhandle','var'), cctv_3_fhandle = figure('Name','cctv_3 plain'); end;
         cctv_3_im_url       = 'http://192.168.1.227:8080/shot.jpg'; 
         cctv_3_im = imread(cctv_3_im_url); 
         cctv_3_info = imfinfo(cctv_3_im_url); 
         cctv_3_data = webread(cctv_3_im_url);   
         figure(cctv_3_fhandle); imshow(cctv_3_im);
         cam_name = 'cctv_3'
         z = 141
         file_name = sprintf('%s%s_%s_%d_%d_%d_%d.%s',file_dir,date_time_str,cam_name,x_,y_,z,theta_,file_suffix)
         imwrite(cctv_3_im, file_name,'Compression','none','Resolution',100) 
        
        
        
start_ros();
cctv_4_im_url       = '/c920_cam/image_raw';   
cctv_4_info_url = '/c920_cam/camera_info';
cctv_4_im_ros_sub = rossubscriber(cctv_4_im_url,'sensor_msgs/Image');
cctv_4_cam_info_ros_sub = rossubscriber(cctv_4_info_url,'sensor_msgs/CameraInfo');
cctv_4_im_obj = receive(cctv_4_im_ros_sub);   cctv_4_im = cctv_4_im_obj.readImage();
        figure(cctv_4_fhandle); imshow(cctv_4_im);
        cam_name = 'cctv_4'
        z = -9000
        file_name = sprintf('%s%s_%s_%d_%d_%d_%d.%s',file_dir,date_time_str,cam_name,x_,y_,z,theta_,file_suffix)
        imwrite(cctv_4_im, file_name,'Compression','none','Resolution',100) 
        cctv_4_im_info = receive(cctv_4_cam_info_ros_sub);      % uncalibrated --> useless
        
        ellapsed_ = toc;
        while ellapsed_ < 0.25 
            pause(0.005); 
            ellapsed_ = toc;
        end
    end
end

