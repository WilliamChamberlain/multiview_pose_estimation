%SurfPointFeature.match Match SURF point features
        %   
        % M = F.match(F2, OPTIONS) is a vector of FeatureMatch objects that 
        % describe candidate matches between the two vectors of SURF 
        % features F and F2.  Correspondence is based on descriptor
        % similarity.
        %
        % [M,C] = F.match(F2, OPTIONS) as above but returns a correspodence
        % matrix where each row contains the indices of corresponding features
        % in F and F2  respectively.
        %
        % Options::
        % 'thresh',T    Match threshold (default 0.05)
        % 'median'      Threshold at the median distance
        %
        % Notes::
        % - for no threshold set to [].
        %
        % See also FeatureMatch.
        
        function [m,corresp] = matchSurfPointFeaturesHellingerRatio(f1, f2, varargin)
%         Duplicate Peter's SurfPointFeature.match, so can then extend and
%         modify it.
            if isempty(f2)
                m = [];
                corresp = [];
                return;
            end

            opt.thresh = 0.05;
            opt.median = false;
            opt.next_match_ratio = 2;
            opt = tb_optparse(opt, varargin);

            % Put the landmark descriptors in a matrix
            D1 = f1.descriptor;
            D2 = f2.descriptor;

            % Find the best matches
            err=zeros(1,length(f1));            
            nextErr=zeros(1,length(f1));
            cor1=1:length(f1); 
            cor2from1=zeros(1,length(f1));
            for i=1:length(f1),
%                 distance = sum( (D2-repmat(D1(:,i),[1 length(f2)])).^2,1);
                distance = sum( 2*sqrt(D2-repmat(D1(:,i),[1 length(f2)])) ,1 ); % Hellinger's Kernel - http://www.vlfeat.org/api/mathop.html , http://www.robots.ox.ac.uk/~vgg/publications/2012/Arandjelovic12/arandjelovic12.pdf 
%                 distance = zeros(1,length(f2),'double');
%                 for d2_idx_=1:length(f2),     % CHECK: larger error is worse: want 1-cosineSimilarity, per pdist( ... ,'cosine')
%                     distance(d2_idx_) = 1 - cosineSimilarity(D1(:,i)',D2(:,d2_idx_)');
%                 end
                [err(i),cor2from1(i)] = min(distance);
                distance(err(i)==distance) = [];
                [nextErr(i), nextCor2(i)] = min(distance);
            end

%             cor2(nextErr(i) > err(i)/2) = [];
%             err(nextErr(i) > err(i)/2) = [];           
            
            % Sort matches on vector distance
            [err, ind] = sort(err); 
            cor1=cor1(ind); 
            cor2from1=cor2from1(ind);
            nextErr = nextErr(ind);

            % Build a list of FeatureMatch objects
            m = [];
            cor = [];
            for i=1:length(f1)
                k1 = cor1(i);
                k2 = cor2from1(i);
                mm = FeatureMatch(f1(k1), f2(k2), err(i));
                m = [m mm];
                cor(:,i) = [k1 k2]';
            end
            
            % if the ratio of the best and next-best match distances exceeds a threshold ratio, reject the match 
            ratio_exceeded_ = 0 ~= err & (nextErr >= err*opt.next_match_ratio);
            err(ratio_exceeded_) = [];
            cor(:,ratio_exceeded_) = [];
            m(ratio_exceeded_) = [];
            
            % if do not mutually match, remove            
            inv_err=zeros(1,length(f2));            
            inv_cor2from1=1:length(f2); 
            inv_cor1from2=zeros(1,length(f2));
            for i=1:length(m)           % go through the matches
                m(i).p2.descriptor      % the point in image 2
                inv_distance = sum( sqrt(D1-repmat(m(i).p2.descriptor(:,i),[1 length(f1)])) ,1 ); % Hellinger's Kernel - http://www.vlfeat.org/api/mathop.html , http://www.robots.ox.ac.uk/~vgg/publications/2012/Arandjelovic12/arandjelovic12.pdf 
                [inv_err(i),inv_cor1from2(i)] = min(inv_distance); % inv_cor1(i) has the matches from f2 to f1
                if f1(cor(inv_cor1from2(i))(1)).descriptor ~= inv_cor1from2(i) ?????     % from  cor(:,i) = [k1 k2]';
            end
            inv_cor1from2(i) % should have the same index into f1 as  cor(:,i) has from f1 into f2
                        

            % get the threshold, either given or the median of all errors
            if opt.median
                thresh = median(err);
                display(sprintf('Median error threshold = median error = %d', thresh));
            else
                thresh = opt.thresh;
                display(sprintf('Fixed error threshold = %d', thresh));
            end

            % remove those matches over threshold
            if ~isempty(thresh)
                k = err > thresh;
                cor(:,k) = [];
                m(k) = [];
            end

            if nargout > 1
                corresp = cor;
            end
        end