%{
Calibrated, registered fixed cameras.
Robot with calibrated camera.
Robot picks features in field of view, pushes to network.
Cameras match features, push back pose+rays+error_estimates /or/
pose+feature_positions+error_estimates.
    May include multiple matches if the feature is not discriminative enough.
Robot constructs and traverses graph. 
%}


% detect_and_draw_sift_features(image_, figure_handle_,edge_threshold_::10,peak_threshold_)

% base_dir = 'H:\Documents\ownCloud'
base_dir = 'D:\_will\ownCloud'

vl_feat_edge_threshold_default=10;
vl_feat_peak_threshold_default=0;

% im = imread(fullfile(base_dir,'\project_AA1__1_1','robot_3_pose_1 (1).jpg'));
% image_gray = single(rgb2gray(im));
% [features_,descriptors_] = vl_sift(image_gray,'EdgeThresh',vl_feat_edge_threshold_default, 'PeakThresh', vl_feat_peak_threshold_default);


% get the camera poses
% cam_1_pose_1_calc.m  % camera_pose_1_1
camera_pose_1 = camera_pose_1_1;
camera_image_1 = camera_1_pose_1;
% cam_3_pose_2_calc.m  % camera_pose_3_2
camera_pose_3 = camera_pose_3_2;
camera_image_3 = camera_3_pose_2;
% cam_4_pose_1_calc.m  % camera_pose_4_1
camera_pose_4 = camera_pose_4_1;
camera_image_4 = camera_4_pose_1;

% extract features from images
[Features_1,Descriptors_1] = vl_sift(single(rgb2gray(camera_image_1)), 'PeakThresh', 8);
display('SIFTed 1');
[Features_3,Descriptors_3] = vl_sift(single(rgb2gray(camera_image_3)), 'PeakThresh', 8);
display('SIFTed 3');
[Features_4,Descriptors_4] = vl_sift(single(rgb2gray(camera_image_4)), 'PeakThresh', 8);
display('SIFTed 4');

% remove features near edges (distortion) - better earlier i.e. trim images
% remove features with poor strength - in grids to enforce span across image - and nearest neighbour

% match features across images pairwise
[matches_1to3,scores_1to3] = vl_ubcmatch(Descriptors_1,Descriptors_3);
display('matched 1 to 3');
[matches_1to4,scores_1to4] = vl_ubcmatch(Descriptors_1,Descriptors_4);
display('matched 1 to 4');
[matches_3to4,scores_3to4] = vl_ubcmatch(Descriptors_3,Descriptors_4);
display('matched 3 to 4');

vlfeat_sift_mosaic(camera_image_1,camera_image_3); % 2175 matches, inliers 8-13 percent mostly on the whiteboard writing, alignment poor
vlfeat_sift_mosaic_peakthreshold(camera_image_1,camera_image_3); 

% % 3D scene structure: positions of features
% feature_positions_1to3 = false;
% feature_positions_1to4 = false;
% feature_positions_3to4 = false;

% % Robot pose
% [R,T,Xc,best_solution]=efficient_pnp(observed_world_coordinates, observed_cam_coordinates, cameraParams.IntrinsicMatrix');  % [R,T,Xc,best_solution]=efficient_pnp(x3d_h,x2d_h,A)
% %Transform = [R T; 0 0 0 1];
% robot_camera_pose = -(R')*T;

surf1 = isurf(camera_1_pose_1);
surf1(1)
idisp(camera_1_pose_1,'dark');
surf1(1:50:end).plot_scale('g','clock');
surf1(1:200:end).plot_scale('g','clock');