
[matches_spf_m, correspondences_spf_m_a] = SurfPointFeature_1.match(SurfPointFeature_2, 'median');
figure; idisp({robot_image,camera_image},'title','Robotic Vision Toolbox SurfPointFeature.match');
matches_spf_m.plot()

% [matches_spf_m_b, correspondences_spf_m_b] = SurfPointFeature.match(SurfPointFeature_1,SurfPointFeature_2,'median');
% idisp({robot_image,camera_image});
% matches_spf_m_b.plot()

[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesEuc(SurfPointFeature_1,SurfPointFeature_2,'median');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Euclidean Difference no Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));

size(matches_spf_m)
size(matches_mspf)

size(matches_spf_m.p1 - matches_mspf.p1);
diff_of_matches = matches_spf_m.p1 - matches_mspf.p1;

figure
subplot(2,2,1)
plot(diff_of_matches(1,:),diff_of_matches(2,:),'gx')

subplot(2,2,3)
histogram(diff_of_matches(1,:))

subplot(2,2,4)
histogram(diff_of_matches(2,:))



[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesEuclideanRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',3);
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Euclidean Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));



[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesHellingerRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1);
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Hellinger Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));



% very slow because of inner loop  
[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesCosineRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1);
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));



[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesHellingerRatioMutual(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1);
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Hellinger Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));





% very slow because of inner loop  
[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesCosineRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1,'distance_metric','euclidean');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));
% display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));


[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesCosineRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1,'distance_metric','hellinger');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));
% display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));


[matches_mspf, correspondences_mspf] = matchSurfPointFeaturesCosineRatio(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf))); 
% display(sprintf(' size(matches_spf_m)=%d , size(matches_mspf)=%d ', size(matches_spf_m),size(matches_mspf)));

[matches_mspf, correspondences_mspf] = matchFeatures(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',2,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));



[matches_mspf, correspondences_mspf] = matchFeatures(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.1,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));


[matches_mspf, correspondences_mspf] = matchFeatures(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.01,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));


[matches_mspf, correspondences_mspf] = matchFeatures(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.001,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));


[matches_mspf, correspondences_mspf] = matchFeatures(SurfPointFeature_1,SurfPointFeature_2,'median','next_match_ratio',1.0001,'distance_metric','cosine');
figure; idisp({robot_image,camera_image},'title','matchSurfPointFeatures Cosine Difference and Match Ratio');
matches_mspf.plot();
display(sprintf(' size(matches_mspf)=%d', size(matches_mspf)));



A = ones(1,3);
A_ = A;
cosSim = zeros(4,21,'double');
for i_ = -10:1:10 
    A_(1) = i_;
    cosSim(1,i_+1+10) = A(1);
    cosSim(2,i_+1+10) = A_(1);
    cosSim(3,i_+1+10) = cosineSimilarity(A,A_);    
    cosSim(4,i_+1+10) = cosineSimilarity(A_,A);
end
cosSim
