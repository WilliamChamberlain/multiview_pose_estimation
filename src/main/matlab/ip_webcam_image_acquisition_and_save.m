% http://au.mathworks.com/matlabcentral/answers/12036-how-do-i-use-my-smart-phone-camera-as-a-webcam-in-matlab
% Answer by PIYUSH KUMAR on 14 Sep 2015 

% url = 'http://192.168.1.178:8080/shot.jpg'; 
url = 'http://192.168.1.186:8080/shot.jpg';
file_base_dir = '/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_lab_2016_11_14'
file_date = '2016_11_14'
framesAcquired = 6;
while (framesAcquired <= 50)                   % the vedio will work till the 50 video frames, after that the vedio will stop. You can use while(1) for infinite loop
      data = imread(url); 
      framesAcquired = framesAcquired + 1;   
      imshow(data);
      filename = sprintf('%s/webcam_1_%s_%d.tiff', file_base_dir, file_date, framesAcquired)
      imwrite(data, filename,'Compression','none','Resolution',300) 
      filename = sprintf('%s/webcam_1_%s_%d.jpg', file_base_dir, file_date, framesAcquired)
      imwrite(data, filename,'Quality',100) 
      hold off  
end
%stop(vid);    % to stop the video
%flushdata(vid);  % erase the data video
clear all