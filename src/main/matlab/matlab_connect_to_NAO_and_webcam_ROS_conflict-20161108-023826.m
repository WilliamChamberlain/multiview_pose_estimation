
figure
rosinit
rostopic list

%  Subscribe with a callback; should trigger every time the topic publishes
%  Still needs the process running: 
% % image_sub = rossubscriber('/nao_zz_camera', rostype.sensor_msgs_Image, @display_image);
% % bob = 0;
% % while bob<1000  % put a limit on it so it doesn't run out of control
% %     bob  = bob+1;
% %     display(bob)
% %     pause(0.5);
% % end;

% Subscribe; manually check for updates via the receive(subscriber_handle)
% function.
% equivalent is a ROS node which
%  subscribes to one or more topics, and runs an infinite loop around a
%  spinOnce(), gathers data from all of the topics, then does some processing, 
%  finally continuing to the next iteration of the loop.

% image_sub = rossubscriber('/nao_zz_camera', rostype.sensor_msgs_Image);
% % image_sub = rossubscriber('/nao_zz_camera','sensor_msgs/Image');
% % image_info_sub = rossubscriber('/nao_robot/camera/front/camera_info','sensor_msgs/CameraInfo');
% % webcam_image_sub = rossubscriber('/image_raw','sensor_msgs/Image');
% % webcam_image_info_sub = rossubscriber('/camera_info','sensor_msgs/CameraInfo');
% % bob = 0;
% % while bob<1000  % put a limit on it so it doesn't run out of control

image_sub = rossubscriber('/high_cam/image_raw','sensor_msgs/Image');
image_info_sub = rossubscriber('/high_cam/camera_info','sensor_msgs/CameraInfo');
webcam_image_sub = rossubscriber('/low_cam/image_raw','sensor_msgs/Image');
webcam_image_info_sub = rossubscriber('/low_cam/camera_info','sensor_msgs/CameraInfo');

    next_image = receive(image_sub);
    next_image_info = receive(image_info_sub);
    next_image_im = next_image.readImage();
    figure('Name','high camera');
    imshow(next_image.readImage());
    
    webcam_next_image = receive(webcam_image_sub);
    webcam_next_image_info = receive(webcam_image_info_sub);
    webcam_next_image_im = webcam_next_image.readImage();
    figure('Name','low camera');
    imshow(webcam_next_image.readImage());
    
% %     bob  = bob+1;
% %     display(bob)
% %     pause(1);
% % end;

s1_base = isift(next_image_im);          % robot's features
s2_base = isift(webcam_next_image_im);
s1=s1_base;
s2=s2_base;

s1=s1_base(s1_base.scale>median(s1_base.scale));
s2=s2_base(s2_base.scale>median(s1_base.scale));
% size(webcam_next_image_im)./size(next_image_im)

[m1to2easy,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','euclidean','next_match_ratio',1.001,'median');
[m2to1easy,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','euclidean','next_match_ratio',1.001,'median');

[m1to2hard,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','euclidean','next_match_ratio',2,'median');
[m2to1hard,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','euclidean','next_match_ratio',2,'median');

idisp({next_image_im,webcam_next_image_im})
m1to2hard.plot
plot(s1(corresp1to2(1,:)),'gx')
temp = s2(corresp1to2(2,:));
plot( temp.u + size(next_image_im,1)  , temp.v , 'gx')
hold on;
m2to1hard.plot


[H_m1to2,r_H_m1to2] = m2to1hard.ransac(@homography,1e-4);
hold on;
m1to2.inlier.plot('b--')

[H_m1to2,r_H_m1to2] = m1to2.ransac(@homography,1e-4);
ransac(@homography,s1.in ,1e-4) s1.

[H_m2to1,r_H_m2to1] = m2to1.ransac(@homography,1e-4);


m1to2 = matchFeatures(s1,s2,'distance_metric','hellinger','next_match_ratio',1.01,'median');
m2to1 = matchFeatures(s2,s1,'distance_metric','hellinger','next_match_ratio',1.01,'median');
matches_fig = figure('Name','matches');
idisp({next_image_im,webcam_next_image_im})
m1to2.plot()


[H_m1to2,H_m1to2_inliers,r_H_m1to2] = m1to2.ransac(@homography,1e-4);
inliers_H_m1to2_fig = figure('Name','inliers m1to2.ransac(@homography')
idisp({next_image_im,webcam_next_image_im})
m1to2(m1to2.inlier).      
m1to2.inlier.plot()
s1(1).plot_scale

[H_m2to1,r_H_m2to1] = m2to1.ransac(@homography,1e-4);
inliers_H_m1to2_fig = figure('Name','inliers m2to1.ransac(@homography')
idisp({next_image_im,webcam_next_image_im})
m1to2.inlier.plot()


% [F,r] = m1to2.ransac(@fmatrix,1e-3,'verbose','maxTrials',5000, 'maxDataTrials',1000);
[F_m1to2,r] = m1to2.ransac(@fmatrix,1e-4,'verbose','maxTrials',5000, 'maxDataTrials',1000);
inliers_F_m1to2_fig = figure('Name','inliers m1to2.ransac(@fmatrix')
idisp({next_image_im,webcam_next_image_im})
m1to2.inlier.plot()
[F_m2to1,r_m2to1] = m2to1.ransac(@fmatrix,1e-4,'verbose','maxTrials',5000, 'maxDataTrials',1000);
inliers_F_m2to1_fig = figure('Name','inliers m2to1.ransac(@fmatrix')
idisp({next_image_im,webcam_next_image_im})
m2to1.inlier.plot()



% rosshutdown

