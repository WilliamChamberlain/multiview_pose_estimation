% April Tags - to get homography automatically -

% MIT implementation by Dr. Michael Kaess - http://people.csail.mit.edu/kaess/apriltags/
% /mnt/nixbig/build_workspaces/apriltags/tags      # - printable PDFs  
% /mnt/nixbig/build_workspaces/apriltags/src/Homography33.cc

% https://botaohu.wordpress.com/2015/03/10/use-apriltag-in-matlab/ 

% setup terminals - /mnt/nixbig/ownCloud/project_AA1__1_1/__README.txt

% turn the head tracking off - http://doc.aldebaran.com/2-1/nao/nao_freeze.html#nao-freeze
%   doc.aldebaran.com/2-1/nao/nao_life.html

% publish images / video from Nao through python  -  do so that can get
% camera info as well as the image - naoqi bringup only publishes
% low-quality images
% - http://answers.ros.org/question/199294/publish-image-msg/
% - https://github.com/rapyuta/rce/blob/master/rce-core/rce/util/converters/image.py
% - http://answers.ros.org/question/212347/how-to-publish-images-quickly/


try 
    rostopic list;
    display('The MATLAB ROS global node is already running.')
catch ME
    display('The MATLAB ROS global node is not running yet: running rosinit.')
    rosinit
    rostopic list
end;

% robot_image_ros_subscriber = rossubscriber('/robot_cam/nao_camera','sensor_msgs/Image');
% robot_camera_info_ros_subscriber = rossubscriber('/nao_robot/camera/front/camera_info','sensor_msgs/CameraInfo');
% robot_image_ros_subscriber = rossubscriber('/nao_robot/camera/front/image_raw','sensor_msgs/Image');
% robot_camera_info_ros_subscriber = rossubscriber('/nao_robot/camera/front/camera_info','sensor_msgs/CameraInfo');
% cctv_image_ros_subscriber = rossubscriber('/high_cam/image_raw','sensor_msgs/Image');
% cctv_camera_info_ros_subscriber = rossubscriber('/high_cam/camera_info','sensor_msgs/CameraInfo');
cctv_image_ros_subscriber = rossubscriber('/cctv_cam_1/image_raw','sensor_msgs/Image');
cctv_camera_info_ros_subscriber = rossubscriber('/cctv_cam_1/camera_info','sensor_msgs/CameraInfo');

robot_ipwebcam_url = 'http://192.168.1.186:8080/shot.jpg';
robot_ipwebcam_image = imread(robot_ipwebcam_url); 
    idisp(robot_ipwebcam_image)
robot_ipwebcam_info = imfinfo(robot_ipwebcam_url);
% robot_ipwebcam = ipcam(robot_ipwebcam_url);   %  TODO - needs a video URL, not a shot.jpg
robot_ipwebcam_data = webread(robot_ipwebcam_url);

% idisp(robot_ipwebcam_image);

%     % can't get the resolution up
%     next_robot_image = receive(robot_image_ros_subscriber);
%     next_robot_image_info = receive(robot_camera_info_ros_subscriber);
%     next_robot_image_camera_param_matrix=reshape(next_robot_image_info.K,3,3)';
%     next_robot_image_im = next_robot_image.readImage();
%     fig_handle = figure('Name','robot camera');
%     idisp(next_robot_image_im, 'figure', fig_handle);
    
    cctv_image = receive(cctv_image_ros_subscriber);
    cctv_image_im = cctv_image.readImage();
%     cctv_image_im = cctv_image_im(1:2:end,1:2:end,:);  % subsample
%     cctv_image_im = imresize(cctv_image_im,0.5);
    fig_handle = figure('Name','fixed camera');      idisp(cctv_image_im, 'figure', fig_handle);
%     HSV image idisp(rgb2hsv(cctv_image_im(413:641,556:1045,:)))
      filename = '/mnt/nixbig/ownCloud/project_AA1__1_1/Phase_1_pics_lab_2016_11_14/cctv_x_1.tif';
      imwrite(cctv_image_im, filename,'Compression','none','Resolution',300) 
      
    cctv_image_info = receive(cctv_camera_info_ros_subscriber);
    cctv_image_camera_param_matrix=reshape(cctv_image_info.K,3,3)';
    
    homography_Matrix_ = homography_calculate_and_apply();
    [rectifiedImage,offset] = homwarp(homography_Matrix_, cctv_image_im, 'full'  );
    fig_handle = figure('Name','rectified fixed camera');
    idisp(rectifiedImage, 'figure', fig_handle)

    
    %% robot looks at feet / observes the world as it traverses - e.g. Patrick Ross, Steve Martin  
    
    %% analyse floor colour/texture
% % %     floorpatch=cctv_image_im(245:475,175:285,:);
% % %     fig_handle = figure;
% % %     idisp([floorpatch(:,:,1)], 'figure', fig_handle)
% % %     histogram(floorpatch(:,:,3)./floorpatch(:,:,1),200) % B/R = 1.5 - 2.5 ; mostly 1.8-2.2
% % %     histogram(floorpatch(:,:,2)./floorpatch(:,:,1),200) % G/R = mostly 1.5 - 2.5, some 0.5 - 1.5 ; 1.0-1.1 and 1.9-2.1 
% % %    
% % %     cctv_image_im_floor =  cctv_image_im; 
% % %     imshow(cctv_image_im_floor);
% % %     cctv_image_im_floor_hsv = rgb2hsv(cctv_image_im);
% % %     floorpatch=cctv_image_im_floor_hsv(245:475,175:285,:);
% % %     histogram(cctv_image_im_floor_hsv(:,:,1),200) % B/R = 60 < H < 100
% % %     histogram(floorpatch(:,:,1),200) % B/R = 60 < H < 100
% % %     size(cctv_image_im)
% % %     floormask = 255*ones(size(cctv_image_im),'uint8');
% % %     imshow(floormask)
    
    
    %% 
    % segment floorspace by HSV : sparse, but closing or counting proportion 
    % per unit area will help
    channel1Min = 0.545;        %  TODO - hardcoding - illumination invariance from floor appearance model as viewed by robot
    channel1Max = 0.57;         %  TODO - hardcoding
    satMin = 0.20;              %  TODO - hardcoding    
    satMax = 0.55;              %  TODO - hardcoding
    hsvRectifiedImage = rgb2hsv(rectifiedImage);
    thresholdedHsvRectifiedImage = (hsvRectifiedImage(:,:,1) >= channel1Min ) & (hsvRectifiedImage(:,:,1) <= channel1Max) & (hsvRectifiedImage(:,:,2) >= satMin ) & (hsvRectifiedImage(:,:,2) <= satMax);
    freeFloorMask = thresholdedHsvRectifiedImage;    
    freeFloorMask = ~freeFloorMask;
    maskedRGBImage = rectifiedImage;    
%     maskedRGBImage = next_robot_image_im;
    maskedRGBImage(repmat(~freeFloorMask,[1 1 3])) = 0;
    figure('Name','Floor free space in fixed camera view');
    imshow(maskedRGBImage);
    
next_robot_image_im =  robot_ipwebcam_image;

s1_base = isift(next_robot_image_im);           % robot's features
s2_base = isift(cctv_image_im);           % fixed camera's features
s1=s1_base;
s2=s2_base;
s1=s1_base(s1_base.scale>median(s1_base.scale)); % remove the fine-scale features: TO INVESTIGATE - control the octaves, or downsample?
s2=s2_base(s2_base.scale>median(s1_base.scale)); % remove the fine-scale features
% size(webcam_next_image_im)./size(next_image_im)

% [m1to2easy,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','euclidean','next_match_ratio',1.001,'median');
% [m2to1easy,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','euclidean','next_match_ratio',1.001,'median');

[m1to2hard,corresp1to2]  = matchFeatures(s1,s2,'distance_metric','euclidean','next_match_ratio',2,'median');
[m2to1hard,corresp2to1]  = matchFeatures(s2,s1,'distance_metric','euclidean','next_match_ratio',2,'median');

f_handle = figure;
idisp({next_robot_image_im,cctv_image_im},'figure',f_handle)
hold on;
m1to2hard.plot('m')
plot(s1(corresp1to2(1,:)),'gx')
% temp = s2(corresp1to2(2,:));
% hold on; plot( temp.u + size(next_image_im,1)  , temp.v , 'gx')
f_handle = figure;
hold off; idisp({cctv_image_im,next_robot_image_im},'figure',f_handle)
hold on;  m2to1hard.plot('y')
plot(s2(corresp2to1(1,:)),'gx')
        % temp = s2(corresp2to1(2,:));
        % hold on; plot( temp.u + size(webcam_next_image_im,1)  , temp.v , 'gx')
f_handle = figure;
[H_m1to2,r_H_m1to2] = m1to2hard.ransac(@homography,1e-2,'maxTrials',5000, 'maxDataTrials',500);
hold off; idisp({next_robot_image_im,cctv_image_im},'title','m1to2hard.ransac(@homography,1e-2','figure',f_handle)
hold on;  m1to2hard.plot('m')
plot(s1(corresp1to2(1,:)),'gx')
hold on;
m1to2hard.inlier.plot('g--')
good_H_m1to2 = H_m1to2;
good_m1to2hard = m1to2hard;

f_handle = figure;
[H_m2to1,r_H_m2to1] = m2to1hard.ransac(@homography,1e-2,'maxTrials',5000, 'maxDataTrials',500);
hold off; idisp({cctv_image_im,next_robot_image_im},'title','m2to1hard.ransac(@homography,1e-2','figure',f_handle)
hold on;  m2to1hard.plot('y')
plot(s2(corresp2to1(1,:)),'bx')
hold on;
m2to1hard.inlier.plot('b--')
good_H_m2to1 = H_m2to1;
good_m2to1hard = m2to1hard;

% cam1 = CentralCamera('image',next_robot_image,'resolution',[640,480],'distortion',[0.084930 -0.153198 0.011283 -0.000882 0.000000],'focal',3.67*0.001);
cam1 = CentralCamera('image',next_robot_image,'sensor', [2.50e-3 , 3.33e-3],'focal',3.67*0.001);
S=cam1.invH(H_m1to2);
S.n
S.T
S(1).T
tr2rpy(S.T(2))

cam1 = CentralCamera('image',next_robot_image,'sensor', [2.50e-3 , 3.33e-3],'distortion',[0.084930 -0.153198 0.011283 -0.000882 0.000000],'focal',3.67*0.001);
S=cam1.invH(H_m1to2);
S.n
S.T
S(1).T
tr2rpy(S.T(2))

% known_separation_y = -121; % -(175-25)/100;
known_separation_z = 107; 

m1to2 = m1to2hard;
[F_m1to2,r] = m1to2.ransac(@fmatrix,1e-2,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({next_robot_image_im,cctv_image_im},'title','inliers m1to2.ransac(@fmatrix', 'figure', inliers_F_m1to2_fig)
m1to2.inlier.plot();
E = cam1.E(F_m1to2);
                sol = cam1.invE(E, [0,0,10]');
                [R,t] = tr2rt(sol);
%                 t = known_separation_y * t/t(1);                           %  !  prior - 1m between photos
                t = known_separation_z * t/t(3);
                T = rt2tr(R, t);
                [R,t] = tr2rt(T);
                rpy_(iteration_+j_,:) = tr2rpy(R, 'deg');
                translation_(:,iteration_+j_) = t;          
t



% [F,r] = m1to2.ransac(@fmatrix,1e-3,'verbose','maxTrials',5000, 'maxDataTrials',1000);
[F_m1to2,r] = m1to2.ransac(@fmatrix,1e-4,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig2 = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({next_robot_image_im,cctv_image_im},'title','inliers m1to2.ransac(@fmatrix', 'figure', inliers_F_m1to2_fig2)
m1to2.inlier.plot();
[F_m2to1,r_m2to1] = m2to1.ransac(@fmatrix,1e-4,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m2to1_fig = figure('Name','inliers m2to1.ransac(@fmatrix');
idisp({next_robot_image_im,cctv_image_im},'title','inliers m2to1.ransac(@fmatrix', 'figure', inliers_F_m2to1_fig)
m2to1.inlier.plot();

[F_m1to2,r] = m1to2.ransac(@fmatrix,1e-3,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m1to2_fig3 = figure('Name','inliers m1to2.ransac(@fmatrix');
idisp({next_robot_image_im,cctv_image_im},'title','inliers m1to2.ransac(@fmatrix,1e-3', 'figure', inliers_F_m1to2_fig3)
m1to2.inlier.plot();
[F_m2to1,r_m2to1] = m2to1.ransac(@fmatrix,1e-3,'verbose','maxTrials',10000, 'maxDataTrials',1000);
inliers_F_m2to1_fig = figure('Name','inliers m2to1.ransac(@fmatrix');
idisp({next_robot_image_im,cctv_image_im},'title','inliers m2to1.ransac(@fmatrix,1e-3','figure',inliers_F_m2to1_fig)
m2to1.inlier.plot();



% rosshutdown

