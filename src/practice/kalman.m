
function kalman
    x__state_estimate_current       = [0;0;0];
    P__state_estimate_covar_current = [0;0;0];
    x__state_estimate_predicted     = [0;0;0];
    P__state_estimate_covar_predicted = [];
    x__state_estimate_updated         = [];
    P__state_estimate_covar_updated   = [];
    u__control_vector               = [0;0];
    F__state_transition_model       = [];
    B__input_response_model         = [];
%     w__process_noise = [];        % wikipedia; w_k is the process noise which is assumed to be drawn from a zero mean multivariate normal distribution with covariance Q_k  Q__process_noise_covar
    Q__process_noise_covar = [];
    H__sensor_measurement_model = [];
    z__measurement_vector = [];
%     v__measurement_noise = [];      % wikipedia; v_k is the observation noise which is assumed to be zero mean Gaussian white noise with covariance R_k
    R__measurement_noise_covar = [];
%     measurement_noise_mean = [];  %     measurement_covar_mean  = z__measurement_vector;  % bzarg : "The distribution has a mean equal to the reading we observed, which we'll call z_arrow_k 
    
    aaa = 1;
    bbb = 0;
    
    robot_position_truth = pose_hist(:,1:2);
    
    for i_ = 1:size(robot_position_truth,1)

        if size(u__control_vector,1) > 0
            [x__state_estimate_predicted, P__state_estimate_covar_predicted] = kf_predict_step(F__state_transition_model, x__state_estimate_current, B__input_response_model, u__control_vector, P__state_estimate_covar_current, Q__process_noise_covar);
            x__state_estimate_current       = x__state_estimate_predicted;
            P__state_estimate_covar_current = P__state_estimate_covar_predicted;
            u__control_vector = [];
            
            u__control_vector_hist           = [u__control_vector_hist; u__control_vector];
            x__state_estimate_predicted_hist = [x__state_estimate_predicted_hist;x__state_estimate_predicted];
        else
            u__control_vector_hist           = [u__control_vector_hist; zeros(u__control_vector_size)];
            x__state_estimate_predicted_hist = [x__state_estimate_predicted_hist;zeros(size(x__state_estimate_current))];            
        end;
        

        if size(z__measurement_vector,1) > 0
            [x__state_estimate_updated, P__state_estimate_covar_updated] = kf_update_step(z__measurement_vector, R__measurement_noise_covar, H__sensor_measurement_model, x__state_estimate_current, P__state_estimate_covar_current);
            x__state_estimate_current       = x__state_estimate_updated;
            P__state_estimate_covar_current = P__state_estimate_covar_updated;
            z__measurement_vector           = [];
            
            z__measurement_vector_hist     = [z__measurement_vector_hist;z__measurement_vector];
            x__state_estimate_updated_hist = [x__state_estimate_updated_hist;x__state_estimate_updated];
        else
            z__measurement_vector_hist     = [z__measurement_vector_hist;zeros(z__measurement_vector_size)];
            x__state_estimate_updated_hist = [x__state_estimate_updated_hist;zeros(size(x__state_estimate_current))];   
        end;
    
    end;
end








