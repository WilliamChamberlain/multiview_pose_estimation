function [x, y, theta] = track_robots_take_a_step(x, y, theta, vLeft, vRight)
    vel        = (vLeft+vRight)/2;      % robot centre velocity is just the average of the drive velocities
    diff_theta = sin(vRight-vLeft);     % vLeft, vRight, vel are aligned with the x axis, so 
    diff_theta = cos(pi/2 + (vLeft-vRight));
    diff_x     = vel*cos(theta);        % steering angle theta is aligned with x axis: theta = 0 --> diff x = vel   : theta = pi/2 = 90 deg -->  diff x = 0
    diff_y     = vel*sin(theta);        % steering angle theta is aligned with x axis: theta = 0 --> diff y = 0     : theta = pi/2 = 90 deg -->  diff y = vel 
    x = x+diff_x;
    y = y+diff_y;
    theta = theta+diff_theta;
end