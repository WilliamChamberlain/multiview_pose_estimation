function [x__state_estimate_predicted, P__state_estimate_covar_predicted] = kf_predict_step(F__state_transition_model, x__state_estimate_current, B__input_response_model, u__control_vector, P__state_estimate_covar_current, Q__process_noise_covar)
    
    x__state_estimate_predicted       = F__state_transition_model * x__state_estimate_current       +  B__input_response_model * u__control_vector;
    
    P__state_estimate_covar_predicted = F__state_transition_model * P__state_estimate_covar_current*F__state_transition_model' + Q__process_noise_covar; 
end