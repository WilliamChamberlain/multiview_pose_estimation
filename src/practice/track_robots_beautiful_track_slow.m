x=1.0;
y=1.0;
theta=0.001;
vLeft=5;
vRight=vLeft*(1+0.1/50); %5.1;

num_iterations_ = 100;
pause_time_     = 0.01;
initial_pose=[x,y,theta,0.0];
pose_hist = zeros(num_iterations_,4,'double');
input_hist = zeros(num_iterations_,2,'double');
var_hist  = zeros(num_iterations_,3,'double');


% drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0 )    ;


%  quiver3  -  3D  -  https://au.mathworks.com/matlabcentral/newsreader/view_thread/27885
%  drawArrow = ..quiver.. http://stackoverflow.com/questions/25729784/how-to-draw-an-arrow-in-matlab
%   
% x1 = [10 30];
% y1 = [10 30];
% 
% drawArrow(x1,y1); hold on
% 
% x2 = [25 15];
% y2 = [15 25];
% 
% drawArrow(x2,y2)

fig_robot_path = figure('Name','plot robot path');
fig_pose_hist_1_v_2 = figure('Name','plot x,y: pose_hist(:,1) vs pose_hist(:,2)'); 
fig_pose_hist_3 = figure('Name','plot theta: pose_hist(:,3)'); 
fig_var_hist_3 = figure('Name','plot state dynamic variables: var_hist(:,:)'); 


for ii = 1:num_iterations_    
    % increase speed
     vLeft=vLeft+0.1;
     vRight=vRight-0.1;
     input_hist(ii,:) = [vLeft,vRight];
     
    vLeft_ = vLeft;
    vRight_ = vRight;
    if rand > 0.7
        vLeft_ = vLeft_ + randn/5; % noise: Gaussian in range ????        
    end;
    if rand > 0.9 
        vRight_ = vRight_ + randn/5; % noise: Gaussian in range ????
    end;
    
    [x, y, theta_new]  ... 
        = track_robots_take_a_step(x, y, theta, vLeft_, vRight_);
    diff_theta = theta_new - theta;
    theta = theta_new;

    var_hist(ii,:)  = [vLeft, vRight, diff_theta];
    pose_hist(ii,:) = [x, y, theta, ii];
    
    if ii > 1 
%         track the robot from the clean localisation signal
        
%         track the robot from the noisy localisation signal
%         track the robot from the noisy + outliers localisation signal
%         draw to the UI windows
        figure(fig_robot_path);
        hold on; drawAnArrow([pose_hist(ii-1,1),pose_hist(ii,1)] , [pose_hist(ii-1,2),pose_hist(ii,2)]);
        pause(pause_time_);
    end
end
figure(fig_pose_hist_1_v_2);  plot(pose_hist(:,1),pose_hist(:,2));

figure(fig_pose_hist_3);  plot(pose_hist(:,3));

figure(); plot(input_hist(:,:)); hold on; plot(var_hist(:,:))