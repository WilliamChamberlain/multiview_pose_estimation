x=0.0;
y=0.0;
theta=0.0;
vLeft=5;
vRight=6;

initial_pose=[x,y,theta,0.0];
pose_hist = zeros(1000,4,'double');

for ii = 1:5
vel        = (vLeft+vRight)/2;
diff_theta = cos(vLeft-vRight);
diff_x     = vel*cos(theta);
diff_y     = vel*sin(theta);
x = x+diff_x;
y = y+diff_y;
theta = theta+diff_theta;
pose_hist(ii,:) = [x,y,theta,ii];
% increase speed
vLeft=vLeft+0.001;
vRight=vRight-0.002;
end;
figure();
plot(pose_hist(:,1),pose_hist(:,2));
figure();
plot(pose_hist(:,3));