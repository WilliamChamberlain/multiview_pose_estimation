syms xi yi ti xj yj tj xm ym tm assume real
% fails: caps
% xi_e = inv( SE2(xm, ym, tm) ) * inv( SE2(xi, yi, ti) ) * SE2(xj, yj, tj);
xi_e = inv( se2(xm, ym, tm) ) * inv( se2(xi, yi, ti) ) * se2(xj, yj, tj);
% fails: Error using sym/subsref Too many output arguments 
% fk = simplify(xi_e.xyt);
fk = simplify(xi_e);

% and the Jacobian which describes how the function f k varies with respect to ξ i is
% fails: Error using sym/jacobian (line 26) The first argument must be a scalar or a vector.
% jacobian_ = jacobian ( fk, [xi yi ti] );


% fails: Ai = simplify (jacobian_)
% debug
xi_e.xyt
fk(1,:)

% debug 
    jacobian_ = jacobian ( fk(1,:), [xi yi ti] );
    jacobian_
    size(fk)
    reshape(fk,1,9)

jacobian_ = jacobian ( reshape(fk,1,9), [xi yi ti] );
jacobian_
size(jacobian_)
% reshape(jacobian_,3,3)
jacobian_ = jacobian_(7:9,:);
jacobian_
Ai = simplify(jacobian_)
Ai


jacobian_j_ = jacobian ( reshape(fk,1,9), [xj yj tj] );
jacobian_j_ = jacobian_j_(7:9,:);
jacobian_j_
Bj=simplify(jacobian_j_)
Bj

%  We load a simple pose graph, similar to Fig. 6.14, from a data file
%  g20 file format: https://github.com/RainerKuemmerle/g2o/wiki/File-Format-SLAM-2D