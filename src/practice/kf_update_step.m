function [x__state_estimate_updated, P__state_estimate_covar_updated] = kf_update_step(z__measurement_vector, R__measurement_noise_covar, H__sensor_measurement_model, x__state_estimate_current, P__state_estimate_covar_current) 
%     measurement_covar_mean  = z__measurement_vector;  % bzarg : "The distribution has a mean equal to the reading we observed, which we'll call z_arrow_k 
    
    y__measurement_residual = z__measurement_vector - H__sensor_measurement_model*x__state_estimate_current;
    S__covariance_residual  = H__sensor_measurement_model * P__state_estimate_covar_current * H__sensor_measurement_model'  +  R__measurement_noise_covar;
    K__optimal_kalman_gain  = P__state_estimate_covar_current * H__sensor_measurement_model' * inv(S__covariance_residual);
    I__state_estimate_covar_identity = eye(size(K__optimal_kalman_gain,1));
    
    x__state_estimate_updated       = x__state_estimate_current + K__optimal_kalman_gain*y__measurement_residual; 
    
    P__state_estimate_covar_updated = (I__state_estimate_covar_identity - K__optimal_kalman_gain*H__sensor_measurement_model) * P__state_estimate_covar_current;
end