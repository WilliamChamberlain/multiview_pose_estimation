% state

Q=      ...
[ 0; 
  0; 
  0 ;


% state transition/response
A =     ...
[ 1 0 0;
  0 1 0;  
  0 0 1];
  
F__state_transition_model       = A;  


% input 
u_raw = ...
[ vel_l;
  vel_r];

% -->
u_plain =      ...
[       ...
    (vel_l + vel_r)/2 ;
    (vel_l - vel_r)/axle_length  
];
vel_linear  = (vel_l + vel_r)/2 ;
vel_angular = (vel_l - vel_r)/axle_length;
% = 
u =     ...
[ vel_linear
  vel_angular  ];
  
u__control_vector               = u;  


% input response : drive the wheels, this is how the state changes,
% assuming 1) small, linearisable changes (small time periods and/or low
% accelerations) 2) apply translation before rotation in each time period
B =    ... 
[ vel_linear * cos(theta);
  vel_linear * sin(theta);
  vel_angular ];
  
B__input_response_model         = B;  

state_noise_covar_mean = [0;0;0];

state_noise_covar_mag = ... % each state variable variation with the input
[  
    (vel_linear * cos(theta))^2, 0 , cos(theta);
    0, , sin(theta);
    cos(theta), , ;
];
Q__process_noise_covar = state_noise_covar_mag;